<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This converts the 1000s to Ks
 * converts Millions to Ms
 *
 * */


if (!function_exists('number_convert')) {
    function number_convert($number, $decimalPlaces = 2)
    {
        if ($number < 1000) {
            return round($number, $decimalPlaces);
        } elseif ($number < 1000000) {
            return round(($number / 1000), $decimalPlaces) . 'K';
        } elseif ($number >= 1000000) {
            return round(($number / 1000000), $decimalPlaces) . 'M';
        }
    }
}

if (!function_exists('memory_convert')) {
    function memory_convert($size)
    {
        $unit = array('kb', 'mb', 'gb', 'tb', 'pb');
        return @round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . ' ' . $unit[$i];
    }
}


if (!function_exists('monthName')) {
    function monthName($x)
    {
        $month = Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
        return $month[$x];
    }
}


if (!function_exists('monthShortName')) {
    function monthShortName($x)
    {
        $month = Array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec');
        return $month[$x];
    }
}


if (!function_exists('remove_p')) {
    function remove_p($string)
    {


        $string = str_replace('<p>', '', $string);
        $string = str_replace('</p>', '', $string);
        $string = str_replace('<br/>', '', $string);

        return $string;
    }
}
if (!function_exists('textfom')) {
    function textfom($txt)
    {

        $ftext = htmlspecialchars($txt, ENT_QUOTES);
        return str_replace(array("\r\n", "\n"), array("<br />", "<br />"), $ftext);
    }
}

if (!function_exists('br2nl')) {
    function br2nl($string)
    {
        return preg_replace('/\<br(\s*)?\/?\>/i', "\n", $string);
    }
}

if (!function_exists('stringNumbers')) {

    function stringNumbers($x)
    {
        $nwords = array("zero", "one", "two", "three", "four", "five", "six", "seven",
            "eight", "nine", "ten", "eleven", "twelve", "thirteen",
            "fourteen", "fifteen", "sixteen", "seventeen", "eighteen",
            "nineteen", "twenty", 30 => "thirty", 40 => "forty",
            50 => "fifty", 60 => "sixty", 70 => "seventy", 80 => "eighty",
            90 => "ninety");


        if (!is_numeric($x))
            $w = '#';
        else if (fmod($x, 1) != 0)
            $w = '#';
        else {
            if ($x < 0) {
                $w = 'minus ';
                $x = -$x;
            } else
                $w = '';
            // ... now $x is a non-negative integer.

            if ($x < 21)   // 0 to 20
                $w .= $nwords[$x];
            else if ($x < 100) {   // 21 to 99
                $w .= $nwords[10 * floor($x / 10)];
                $r = fmod($x, 10);
                if ($r > 0)
                    $w .= ' ' . $nwords[$r];
            } else if ($x < 1000) {   // 100 to 999
                $w .= $nwords[floor($x / 100)] . ' hundred';
                $r = fmod($x, 100);
                if ($r > 0) {
                    $w .= ' and ';
                    if ($r < 100) {   // 21 to 99
                        $w .= $nwords[10 * floor($r / 10)];
                        $r = fmod($r, 10);
                        if ($r > 0)
                            $w .= ' ' . $nwords[$r];
                    }
                }
            } else if ($x < 1000000) {   // 1000 to 999999
                $w .= $nwords[floor($x / 1000)] . ' thousand';
                $r = fmod($x, 1000);
                if ($r > 0) {
                    $w .= ' ';
                    if ($r < 100)
                        $w .= ' and ';
                    $w .= $nwords[$r];
                }
            } else {    //  millions
                $w .= stringNumbers(floor($x / 1000000)) . ' million';
                $r = fmod($x, 1000000);
                if ($r > 0) {
                    $w .= ' ';
                    if ($r < 100)
                        $w .= ' and ';
                    $w .= int_to_words($r);
                }
            }
        }
        //echo $w;

        return $w;


    }
}


if (!function_exists('Networks')) {


    function Networks($country = 'UG')
    {

        $country = strtoupper($country);

        switch ($country) {
            case 'UG' :

                $u[0]['code'] = '25677';
                $u[0]['title'] = 'MTN_UG';

                $u[1]['code'] = '25678';
                $u[1]['title'] = 'MTN_UG';

                $u[2]['code'] = '25639';
                $u[2]['title'] = 'MTN_UG';


                $u[3]['code'] = '25675';
                $u[3]['title'] = 'AIRTEL_UG';

                $u[4]['code'] = '25670';
                $u[4]['title'] = 'AIRTEL_UG';

                $u[4]['code'] = '25679';
                $u[4]['title'] = 'ORANGE_UG';

                $u[5]['code'] = '25671';
                $u[5]['title'] = 'UTL_UG';
                break;


            case 'RW':


                $u[0]['code'] = '25078';
                $u[0]['title'] = 'MTN_RW';

                $u[1]['code'] = '25073';
                $u[1]['title'] = 'AIRTEL_RW';

                $u[2]['code'] = '25072';
                $u[2]['title'] = 'TIGO_RW';

                $u[3]['code'] = '';
                $u[3]['title'] = 'OTHERS';

                break;


            default:

                $u = array();

                break;


        }
        return $u;
    }
}


if (!function_exists('phone')) {
    function phone($phone)
    {


        if (strstr($phone, ',') or strstr($phone, '/') or strstr($phone, '-')) {
            if (strstr($phone, ',')) {
                $phone = substr($phone, 0, strpos($phone, ','));
            }
            if (strstr($phone, '/')) {
                $phone = substr($phone, 0, strpos($phone, '/'));
            }
            if (strstr($phone, '-')) {
                $phone = substr($phone, 0, strpos($phone, '-'));
            }
            $phone = trim($phone);
        }


        if (strlen($phone) >= 9) {

            if (substr($phone, 0, 1) == '0') {
                if (strlen($phone) == 10) {
                    $phone = '250' . substr($phone, 1);
                }
            }
            if (strlen($phone) == 9 and substr($phone, 0, 1) !== 0) {
                $phone = '250' . $phone;
            }
            if (substr($phone, 0, 1) == '+') {
                $phone = substr($phone, 1);
            }


            return $phone;

        }else{
            return 'Phone Number is Invalid';
        }

    }
}


if (!function_exists('phone2')) {
    function phone2($phone)
    {
        if (strstr($phone, '+')) {
//                776716138

            $phone = substr($phone, 1);
            $mobile = strlen($phone) == 10 ? '250' . substr($phone, 1) : $phone;
            return $mobile;
        } else {
            $mobile = strlen($phone) == 9 ? '250' . $phone : $phone;
            return $mobile;
        }
    }
}


if (!function_exists('trending_date')) {
    function trending_date($date)
    {
        return date('Y-m-d') == date('Y-m-d', $date) ? date('H:i:s', $date) : date('d-m-Y', $date);
    }
}
if (!function_exists('trending_date_time')) {
    function trending_date_time($date)
    {
        return date('Y-m-d') == date('Y-m-d', $date) ? date('H:i:s', $date) : date('d-m-Y H:i', $date) . 'hrs';
    }
}

if (!function_exists('code')) {
    function code($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}

if (!function_exists('hashValue')) {
    function hashValue($v)
    {

        $mypassword = $v;
        $mypassword = crypt($mypassword, sha1(md5($mypassword)));
        $v = crypt($mypassword, $mypassword); // let the salt be automatically generated

        return $v;
    }
}

if (!function_exists('readJsonFgets')) {
    function readJsonFgets()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        return $data;
    }
}
if (!function_exists('readJson')) {

    function readJson($obj, $type = 'url')
    {
        if ($type == 'url') {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_URL, $obj);
            $result = curl_exec($ch);
            curl_close($ch);
        } else {
            $result = $obj;
        }

        return $obj = json_decode($result);

    }
}


if (!function_exists('local_server')) {
    function local_server()
    {
        if ($_SERVER["SERVER_ADDR"] == "127.0.0.1" || $_SERVER["SERVER_ADDR"] == "localhost" || $_SERVER["SERVER_ADDR"] == "http://localhost/" || $_SERVER["SERVER_ADDR"] == "::1") {
            return True;
        } else {

            return False;
        }
    }
}


if (!function_exists('url_get_contents')) {

    function url_get_contents($url)
    {
        if (function_exists('curl_exec')) {
            $conn = curl_init($url);
            curl_setopt($conn, CURLOPT_SSL_VERIFYPEER, true);
            curl_setopt($conn, CURLOPT_FRESH_CONNECT, true);
            curl_setopt($conn, CURLOPT_RETURNTRANSFER, 1);
            $url_get_contents_data = (curl_exec($conn));
            curl_close($conn);
        } elseif (function_exists('file_get_contents')) {
            $url_get_contents_data = file_get_contents($url);
        } elseif (function_exists('fopen') && function_exists('stream_get_contents')) {
            $handle = fopen($url, "r");
            $url_get_contents_data = stream_get_contents($handle);
        } else {
            $url_get_contents_data = false;
        }
        return $url_get_contents_data;
    }

}

if (!function_exists('week_no')) {
    function week_no($ddate)
    {

        //$ddate = date('Y-m-d');
        $date = new DateTime($ddate);
        $week = $date->format("W");

        return $week;
    }

}

if (!function_exists('getStartAndEndDate')) {

    function getStartAndEndDate($week, $year)
    {
        $week_start = new DateTime();
        $week_start->setISODate($year, $week);
        $return[0] = $week_start->format('Y-m-d');
        $time = strtotime($return[0], time());
        $time += 6 * 24 * 3600;
        $return[1] = date('Y-m-d', $time);
        return $return;
    }
}


if (!function_exists('getNetwork')) {
    function getNetwork($mobile)
    {
        $substring = substr($mobile, 0, 5);

        if ($substring == "25678" || $substring == "25677" || $substring == "25639") {
            return "MTN_UG";

        } else if ($substring == "25675" || $substring == "25670") {

            return "AIRTEL_UG";

        } else if ($substring == "25679") {

            return "AFRICELL_UG";

        } else if ($substring == "25671") {

            return "UTL_UG";
        } else if ($substring == "25078") {

            return "MTN_RW";
        } else if ($substring == "25073") {

            return "AIRTEL_RW";

        } else if ($substring == "25072") {

            return "TIGO_RW";

        } else {
            return "RWANDACELL";
        }
    }
}


if (!function_exists('json_output')) {
    function json_output($data = null)
    {

        header('Content-Type:application/json');

        $no_output = array(
            'response' => 'error',
            'message' => 'Nothing Encoded'
        );

        $response = isset($data) || count($data) > 0 ? $data : $no_output;

        echo json_encode($response, JSON_PRETTY_PRINT);

    }
}


if (!function_exists('change_destination')) {

    function change_destination($destination, $duration = 2)
    {
        echo '<META http-equiv=refresh content=' . $duration . ';URL=' . base_url() . 'index.php/' . $destination . '>';
    }
}


if (!function_exists('extract_file')) {
    function extract_file($file)
    {


        $output = array();
        while (!feof($file)) {

            $dd = fgets($file);

            array_push($output, $dd);
//                    $parts = explode('"', $dd);
//                    $statusCode = substr($parts[2], 0, 4);

        }

        return $output;
    }
}


if (!function_exists('get_string_diff')) {
    function get_string_diff($old, $new)
    {
        $from_start = strspn($old ^ $new, "\0");
        $from_end = strspn(strrev($old) ^ strrev($new), "\0");

        $old_end = strlen($old) - $from_end;
        $new_end = strlen($new) - $from_end;

        $start = substr($new, 0, $from_start);
        $end = substr($new, $new_end);
        $new_diff = substr($new, $from_start, $new_end - $from_start);
        $old_diff = substr($old, $from_start, $old_end - $from_start);

        return strlen($new_diff) != strlen($old_diff) ? true : false;


        //$new = "$start<ins style='background-color:#ccffcc'>$new_diff</ins>$end";
        //$old = "$start<del style='background-color:#ffcccc'>$old_diff</del>$end";
        //return array("old"=>$old, "new"=>$new);
    }

}

if (!function_exists('fwrite_stream')) {
    function fwrite_stream($fp, $string)
    {
        for ($written = 0; $written < strlen($string); $written += $fwrite) {
            $fwrite = fwrite($fp, substr($string, $written));
            if ($fwrite === false) {
                return $written;
            }
        }
        return $written;
    }
}


if (!function_exists('sms_time')) {

    function sms_time($time = null)
    {
        if (isset($time)) {
            $year = $substring = substr($time, 0, 2);
            $month = $substring = substr($time, 2, 2);
            $date = $substring = substr($time, 4, 2);
            $hour = $substring = substr($time, 6, 2);
            $min = $substring = substr($time, 8, 2);

            return $old_date = mktime($hour, $min, 00, $month, $date, $year);
        } else {
            return '-';
        }

    }
}


if (!function_exists('split_date')) {
    function split_date($date_range = null, $separator = '~')
    {

        $dates = explode($separator, $date_range);

        return array(
            'date1' => trim($dates[0]),
            'date2' => trim($dates[1])
        );

    }
}


if (!function_exists('period')) {
    function period($fdate = null, $last_day = null)
    {


        $fdate = isset($fdate) ? date($fdate) : date('Y-m-01 00:00:01');
        $ldate = isset($last_day) ? date($last_day) : date('Y-m-d 23:59:59');

        $begin = new DateTime($fdate);
        $end = new DateTime($ldate);

        $interval = DateInterval::createFromDateString('1 day');
//$period = new DatePeriod($begin, $interval, $end->modify( '+1 day' ));

        $add_aday = isset($week_no) ? 0 : 1;//will determine wetha to add a day or not
        return new DatePeriod($begin, $interval, $end->modify("+$add_aday day"));

    }
}


if (!function_exists('message_count')) {

    function message_count($string = null, $single_sms = 160)
    {

        if (isset($string)) {

            $string = strlen($string);

            $actual_count = $string / $single_sms;

            return ceil($actual_count);

        } else {
            return false;
        }

    }
}


if (!function_exists('remove_space')) {
    function remove_space($string)
    {
        $pattern = '/\s+/';
        $replacement = '';
//            return preg_replace('/\s+/', '', $string);
        return preg_replace($pattern, $replacement, $string);

    }
}




if (!function_exists('my_count')) {

    function my_count($value)
    {
        $r=array();

        if (is_object($value)) {

            $r = json_decode(json_encode($value), true);

        } elseif (is_countable($value)) {
            $r = $value;
        }

        return (count($r));
    }
}








