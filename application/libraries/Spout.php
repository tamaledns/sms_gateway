<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php
//ini_set('error_reporting', E_STRICT);

require_once APPPATH.'/third_party/Spout/Autoloader/autoload.php';

//lets Use the Spout Namespaces
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;



class Spout{

    public $reader;

    function __construct()
    {
//        parent::__construct();

        $this->reader = ReaderFactory::create(Type::XLSX); //set Type file xlsx
        //$reader = ReaderFactory::create(Type::CSV); // for CSV files
        //$reader = ReaderFactory::create(Type::ODS); // for ODS files

    }

     function example() {


        $this->load->library('spout');

        try {



            $reader=$this->spout->reader;

            //Lokasi file excel
            $file_path = "/Applications/XAMPP/xamppfiles/htdocs/sms_gateway/uploads/groups/test10k2.xlsx";
            $reader->open($file_path); //open the file

            $i = 0;

            /**
             * Sheets Iterator. Kali aja multiple sheets
             **/
            foreach ($reader->getSheetIterator() as $sheet) {




                //Rows iterator
                foreach ($sheet->getRowIterator() as $row) {

                    echo '<br/>';

                    print_r($row);

                    ++$i;
                }
            }

            echo "Total Rows : " . $i;
            $reader->close();


            echo "Peak memory:", (memory_get_peak_usage(true) / 1024 / 1024), " MB";

        } catch (Exception $e) {

            echo $e->getMessage();
            exit;
        }

    }//end of function



}

?>