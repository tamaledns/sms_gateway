<?php
/**
 * Created by PhpStorm.
 * User: DennisTamale
 * Date: 15/02/2018
 * Time: 11:08
 */

class Billing
{

    private $CI;
    private $customer = 7;
    private $customer_sub_account = 9;

    function __construct()
    {

        $this->CI =& get_instance();
        $this->CI->load->database();


    }

    function send_message_true($user_id = null, $single_sms = null, $no_contacts = 1)
    {

        $status = 'error';
        $message = 'error occurred..';

        if (isset($single_sms) && isset($user_id)) {

            $message_count = $no_contacts * $this->message_count($single_sms);

            if ($this->get_account($user_id) != false) {

                $account_details = $this->get_account($user_id);

                $account_type = $this->account_type($user_id);

                if ($account_type->account == 'admin') {

                    //Just Send message without Charging
                    $admin = $account_details;
                    $status = 'success';
                    $message = array(
                        'username' => $admin->username,
                        'balance_p' => $admin->balance_p,
                        'message_bill' => 0
                    );


                } else {
                    //Continue the procedures


                    $balance = $this->account_balance($user_id);
                    $message_bill = $this->message_bill($user_id, $message_count);

                    if ($balance > $message_bill) {

                        if ($account_type->user_type == $this->customer) {
                            //send message

                            $customer = $account_details;
                            $status = 'success';
                            $message = array(
                                'username' => $customer->username,
                                'balance_p' => $customer->balance_p,
                                'message_bill' => $message_bill
                            );


                        } elseif ($account_type->user_type == $this->customer_sub_account) {

                            $sub_account = $account_details;
                            $reseller_id = $sub_account->parent_id;


                            if ($this->get_account($reseller_id) != false) {

                                $reseller = $this->get_account($reseller_id);

                                $reseller_balance = $reseller->balance;//$this->account_balance($reseller_id);

                                if ($reseller_balance > $message_bill) {
                                    //send message

                                    $status = 'success';
                                    $message = array(
                                        'username' => $reseller->username,
                                        'balance_p' => $reseller->balance_p,
                                        'message_bill' => $message_bill
                                    );

                                } else {

                                    ///insuficient balance
                                    $message = 'Error 22 Occurred to your Reseller account Please Contact your Reseller..';
                                }


                            } else {

                                ///reseller account cannot be found
                                $message = 'Error 11 Occurred to your Reseller account Please Contact your Reseller..';
                            }


                        }

                    } else {
                        $message = 'Insufficient credits on your account..';
                    }

                }


            } else {
                $message = 'Account Doesn\'t exist..';
            }


        } else {
            $message = 'some Parameter are missing..';
        }

        $response = array(
            'status' => $status,
            'message' => $message
        );

        return json_decode(json_encode($response));

    }


    //this method subtracts money from the user account
    function debit_account($user_id, $amount)
    {

        $amount = floatval($amount);

        $user = $this->get_account($user_id);

        $sql = "UPDATE `users` SET `balance`=`balance`-$amount WHERE `id`='$user_id';";
        $this->CI->db->query($sql);

        if ($user->user_type == 9) {

            $sql_reseller = "UPDATE `users` SET `balance`=`balance`-$amount WHERE `id`='$user->parent_id';";
            $this->CI->db->query($sql_reseller);
        }


        $this->CI->custom_library->add_logs('', 'billing ', ' User with ID : ' . $user_id . ' has been debited  with ' . $amount);


    }


    function account_balance($user_id = null)
    {

        if ($this->get_account($user_id) != false) {

            $account = $this->get_account($user_id);

            $balance = $account->balance;
            return $balance;
        } else {
            return false;
        }
    }

    function log_responses($string)
    {
        $this->CI->load->helper("file");

        $path = 'logs/';
        if (!is_dir($path)) //create the folder if it's not already exists
        {
            mkdir($path, 0777, TRUE);
        }
        $date = date('dmY');
        $file = "log_response_$date.txt";

        if (!write_file($path . $file, $string . '-' . date('Y-m-d H:i:s ' . PHP_EOL), "a+")) {
            //echo 'Unable to write the file';
        } else {
            //echo 'File written!';
        }

    }

    function message_count($string = null, $single_sms = 160)
    {
        return message_count($string, $single_sms);
    }

    function message_bill($account_id = null, $messages = null)
    {

        $output = false;

        if (isset($account_id) && isset($messages)) {

            $account = $this->get_account($account_id);

            $rate = $account->rate;

            return $messages * $rate;

        }
        return $output;
    }

    function account_type($user_id = null)
    {

        if ($this->get_account($user_id) != false) {

            $account = $this->get_account($user_id);

            $account = array(
                'user_type' => $account->user_type,
                'title' => $account->title,
                'account' => $account->user_type == 1 ? 'admin' : 'other'
            );
            $account = json_decode(json_encode($account));
            return $account;
        } else {
            return false;
        }

    }


    function gateway_balance($username, $password)
    {
        $credentials = array('username' => $username, 'password' => $password);
        $this->CI->load->library('sms_api', $credentials);
        $m = $this->sms_api->balance();
        $m = json_decode($m);

        $data = $m->data;
        return $data->balance;

    }

    function get_account($user_id = null)
    {

        $output = false;

        if (isset($user_id)) {

            $user = $this->CI->db->select('a.*,b.title')->from('users a')->join('user_type b', 'a.user_type=b.id')->where(array('a.id' => $user_id))->get()->row();
            if (my_count($user) > 0) {
                $output = $user;
            }
        }

        return $output;

    }


    function send_sms($phone, $sms, $subject = null, $client_ref_id = null)
    {
        if (!empty($phone)) {

            if (strstr($phone, ',') or strstr($phone, '/') or strstr($phone, '-')) {
                if (strstr($phone, ',')) {
                    $phone = substr($phone, 0, strpos($phone, ','));
                }
                if (strstr($phone, '/')) {
                    $phone = substr($phone, 0, strpos($phone, '/'));
                }
                if (strstr($phone, '-')) {
                    $phone = substr($phone, 0, strpos($phone, '-'));
                }
                $phone = trim($phone);
            }
            if (strlen($phone) >= 9) {
                if (substr($phone, 0, 1) == '0') {
                    if (strlen($phone) == 10) {
                        $phone = '250' . substr($phone, 1);
                    }
                }
                if (strlen($phone) == 9 and substr($phone, 0, 1) !== 0) {
                    $phone = '250' . $phone;
                }
                if (substr($phone, 0, 1) == '+') {
                    $phone = substr($phone, 1);
                }
//                    $q = $this->CI->db->select('id')->from('outbox')->where(array('to_user' => $phone, 'message' => $sms))->limit(1)->get()->result();
//                    if (!isset($q[0]->id)) {


                if ($this->CI->session->user_type == 7) {


                    $username = $this->CI->session->username;
                    $password = str_rot13($this->CI->session->balance_p);

                } else {

                    $username = 'daniel';
                    $password = 'daniel';

                }
                $subject = isset($subject) ? $subject : 'INFOSMS';


                $url_encoded_sms = urlencode($sms);
                $url_to_reply = base_url('send/send_client_reply/');


                $url = $this->sms_base_url . "send?username=$username&password=$password&to=$phone&content=$url_encoded_sms&from=$subject";
                $url .= "&dlr=yes&dlr-level=2&dlr-url=$url_to_reply&dlr-method=GET";
                $socket = url_get_contents($url);

                if ($socket) {

                    //$this->CI->db->insert('outbox','to_user,subject,m_type,message,created_on,created_by',"'$phone','Notification','SMS','$sms','".time()."','0'");

                    $this->CI->db->insert('outbox', array('to_user' => $phone, 'subject' => $subject, 'm_type' => 'SMS', 'message' => $sms, 'created_on' => time(), 'created_by' => $this->CI->session->id));

//                            fclose($socket);
                }
//                    }
            }
        }
    }


}