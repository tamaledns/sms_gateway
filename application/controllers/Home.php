<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class home extends MY_Controller
{
    function __construct()
    {
        parent::__construct();


    }

    public function index($page = null)
    {
        $this->page_level = 'home/';
        $data['title'] = 'home';
        $data['subtitle'] = '';

        $data['page_view']=$this->load->view('home/landing/landing', $data,true);
        $this->load->view($this->page_level . 'landing/main_page', $data);

    }

    public function contact($type = null)
    {

        $data['title'] = $title = $this->uri->segment(2);
        $data['subtitle'] = $type;

        $data['page_view']=$this->load->view('home/landing/contact', $data,true);
        $this->load->view($this->page_level . 'landing/main_page', $data);


    }

    function my_applications($type=null){
        $this->isloggedin()?'':$this->logout();

        $data['title'] = $title = $this->uri->segment(2);
        $data['subtitle'] = $type;
        $root = $this->page_level . $this->page_level2;


        switch ($type) {
            default:


                $data['page_view'] = $this->load->view($root . $title, $data, true);

                break;
        }
        $this->load->view($this->page_level . 'static/main_page', $data);

    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */