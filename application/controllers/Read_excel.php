<?php



/**
 * Excel dengan CI & Spout
 *
 */

class Read_excel extends CI_Controller {

    public function readExcelFile() {


        $this->load->library('spout');

        try {



            $reader=$this->spout->reader;

            //Lokasi file excel
            $file_path = "/Applications/XAMPP/xamppfiles/htdocs/sms_gateway/uploads/groups/test10k2.xlsx";
            $reader->open($file_path); //open the file

            $i = 0;

            /**
             * Sheets Iterator. Kali aja multiple sheets
             **/
            foreach ($reader->getSheetIterator() as $sheet) {

                @ini_set('MAX_EXECUTION_TIME', -1);
                //Rows iterator
                $contacts=array();
                foreach ($sheet->getRowIterator() as $row) {


                    $values = array(

                        'phone_no' => $row[0],
                        'group_id'=>11,
                        'first_name' => isset($row['B'])?$row['B']:'',
                        'created_on' => time(),
                        'created_by' => $this->session->id,

                    );

//                    $contacts=$row[0];
//                    print_r($row);

                    array_push($contacts,$values);

                    ++$i;



                }

//                print_r($contacts);
            }

            echo "Total Rows : " . $i;
            $reader->close();
            count($contacts)>0?$this->db->insert_batch('contacts', $contacts):'';


            echo "Peak memory:", (memory_get_peak_usage(true) / 1024 / 1024), " MB";

        } catch (Exception $e) {

            echo $e->getMessage();
            exit;
        }

    }//end of function


}//end of class

//
//this will output
//
//Array
//(
//    [0] => SPP-16755
//    [1] => 42198
//    [2] => Mester SERVER
//[3] => Rp.9000
//    [4] => Banjarmasin
//)
//....
//....
//
//Total Rows : 6171
//Peak memory usage: 2 MB
//
//Read Big Excel File Using PHP Spout Library