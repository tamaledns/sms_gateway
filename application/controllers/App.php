<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class app extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->isloggedin() == true ? '' : $this->logout();

    }


    public function index()
    {
        //echo 'dennis';

        $this->dashboard();

        //redirect('app/reports');
        //$data['title']='';
        //$data['subtitle']='';

        //$this->load->view('app/static/index',$data);
        // redirect($this->page_level.'stories');

    }


    public function isloggedin()
    {
        return strlen($this->session->userdata('user_type')) > 0 ? true : false;

    }

    public function dashboard()
    {

        $this->load->model('dashboard_model');
        $this->load->library('auto_create_table');
        $data = array(
            'title' => 'dashboard',
            'subtitle' => 'Welcome',
            'link_details' => 'Account overview'
        );
        $page_level = $this->page_level;

        $this->form_validation->set_rules('filter', 'Filter', 'trim');

        if ($this->form_validation->run() == true) {


            $data['date_range'] = $this->input->post('date_range');

            //outputing the days to view
            $dates = split_date($this->input->post("date_range"), '~');

            $data['fdate'] = $fdate = $dates['date1'];
            $data['last_day'] = $ldate = $dates['date2'];
            $data['subtitle'] = 'Date range : ' . $fdate . ' to ' . $ldate;


        } else {
            $data['subtitle'] = 'Summaries';
            $data['fdate'] = date('Y-m-01');
        }

        $data['page_view'] = $this->load->view($page_level . 'dashboard/dashboard', $data, true);


        $this->load->view('app/static/main_page', $data);

    }


    public function messaging2($type = null, $id = null)
    {

        $data = array(
            'title' => $this->uri->segment(2),
            'subtitle' => $type,
            'page_level' => $this->page_level

        );
        $root = $this->page_level . $this->page_level2;


        switch ($type) {

            default:
                $this->load->view($this->page_level . 'static/header', $data);
                $this->form_validation->set_rules('to', 'To', 'xss_clean|trim|required')
                    ->set_rules('subject', 'Subject', 'xss_clean|trim|required');

                if ($this->form_validation->run() == true) {

                    $emails = explode(',', $this->input->post('to'));
                    $subject = $this->input->post('subject');
                    $message = $this->input->post('message');
                    $cc = $this->input->post('cc');
                    $bcc = $this->input->post('bcc');

                    foreach ($emails as $em) {

                        print_r($em);
                        valid_email($em) ? $this->custom_library->sendMail($em, $subject, $message, null, $cc, $bcc) : '';
                    }

                } else {

                }

                $this->load->view($root . 'messaging');
                break;

            case 'app_inbox_reply':
                $data['message_id'] = $id;//$this->input->get('message_id');//$_GET['message_id'];
                // $file_name=$this->uri->segment(5);;
                $this->load->view($root . $type, $data);
                break;
            case 'inbox':

                if (strlen($id) > 0) {
                    if ($id == 'Emails') {
                        $message_group = 'Email';
                    } elseif ($id == 'SMS') {
                        $message_group = 'SMS';
                    }
                }


                $this->db->select()->from('inbox');

                isset($message_group) ? $this->db->where(array('m_type' => $message_group)) : '';

//                    $this->session->userdata('user_type')==1?'':$this->db->where(array('company_id'=>$this->session->company));

                $data['message'] = $this->db->order_by('id', 'desc')->limit(25)->get()->result();

                $this->load->view($root . $type, $data);
                break;
            case 'outbox':

                if (strlen($id) > 0) {
                    if ($id == 'Emails') {
                        $message_group = 'Email';
                    } elseif ($id == 'SMS') {
                        $message_group = 'SMS';
                    }
                }


                $this->db->select()->from('outbox');

                isset($message_group) ? $this->db->where(array('m_type' => $message_group)) : '';

//                    $this->session->user_type==1?'':$this->db->where(array('company_id'=>$this->session->company));

                $data['message'] = $this->db->order_by('id', 'desc')->limit(25)->get()->result();

                $this->load->view($root . $type, $data);
                break;
            case 'app_inbox_compose':

//                    $data=$this->input->post();
                $this->load->view($root . $type);
                break;
            case 'app_inbox_view':
                $data['message_id'] = $id;//$this->input->get('message_id');//$_GET['message_id'];
                $file_name = $this->uri->segment(5);;
                $file = "app_" . $file_name . "_view";
                $this->load->view($root . $file, $data);
                break;


        }


        // $this->load->view($this->page_level . 'footer', $data);

    }

    function message_count(){

        $string="Dear pretty boys. Start working hard and get loaded. We are tired of dating ugly men because of money. 
Now my donkey Salim Swaleh is cute, hair all over his face in the right places, the eyebrows will make you think some caterpillars are mating on his face at first glance. Check the nails, very on point you'd think it's not his hand.(such well kept fingers don't count money.mmsteww even) Talina work. I ended up with some Sugar daddy that looks like abiriga because he can afford Javas. Now see. The food he was slaying with?";
        echo message_count($string);
    }



}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */