<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class excel_export extends CI_Controller
{

    public $page_level = "";
    public $page_level2 = "";
    public $page_level3 = "";
    private $acronym = 'PMIS';
    private $site_name='';
    public $inbox_table;
    public $out_table;


    function __construct()
    {
        parent::__construct();

        $this->isloggedin() == true ? '' :$this->invalid();
        $this->load->library('excel');

        $this->page_level = $this->uri->segment(1);
        $this->page_level2 = $this->uri->segment(2);
        $this->page_level3 = $this->location_title= $this->uri->segment(3);
        $this->site_name=$this->site_options->title('site_name');

        $this->inbox_table = 'inbox_' . date('dmY');
        $this->out_table = 'outbox_' . date('dmY');
        $this->create_inbox_table();

    }

    function create_inbox_table($date=null)
    {
        $this->load->library('auto_create_table');

        isset($date)?$this->auto_create_table->create_inbox_table($date):$this->auto_create_table->create_inbox_table();

    }



    function invalid(){

        echo "You can't access this report Contact administrator";
        exit;
    }

    public function isloggedin()
    {
        return $this->session->userdata('user_type') == 1 || $this->session->userdata('user_type') == 2 || $this->session->userdata('user_type') == 3 || $this->session->userdata('user_type') == 4 || $this->session->userdata('user_type') == 5|| $this->session->userdata('user_type') == 6 ? true : false;

    }


    function index()
    {
        echo phpinfo();
    }

    function messages($type=null,$year){

        //this is the part where the export is initialised

        $objPHPexcel = PHPExcel_IOFactory::load('excel_templates/template.xlsx');
        $objWorksheet = $objPHPexcel->getActiveSheet(0);
        ///Exports depends on the level


        $header = array('USERNAME', 'MSISDN', 'NETWORK', 'SENDER-ID', 'MESSAGE','CLIENT-REF','SMSC-STATUS','SENT-DATE','SENT-TIME','DELIVERY-DATE','DELIVERY-TIME');

        $L='A';
        $hno=1;
        foreach ($header  as $h){
            $objWorksheet->getCell($L.$hno)->setValue($h);
            $L++;
        }
// Data

        $no = 2;


        if (isset($_REQUEST['message_date']) && strlen($_REQUEST["message_date"]) > 0 ) {
            $filter_date=$_REQUEST['message_date'];

            $filter_table=$this->inbox_table = 'inbox_' . $filter_date;

            $this->create_inbox_table($filter_table);
        }

        if (isset($year) && ($year == 2015 || $year == 2016 || $year == 2017)) {

            $message_table = 'inbox' . $type . '_' . $year;

        } else {

            $message_table = $this->inbox_table;
        }



        $this->db->select()->from($message_table . ' a');
        isset($_REQUEST['network']) && strlen($_REQUEST["network"]) > 0 ? $this->db->where(array('a.network' => $_REQUEST["network"])) : '';
        isset($_REQUEST['username']) && strlen($_REQUEST["username"]) > 0 ? $this->db->where(array('a.username' => $_REQUEST["username"])) : '';
        isset($_REQUEST['msisdn']) && strlen($_REQUEST["msisdn"]) > 0 ? $this->db->where(array('a.phone' => $_REQUEST["msisdn"])) : '';
        isset($_REQUEST['sender_id']) && strlen($_REQUEST["sender_id"]) > 0 ? $this->db->like('a.from', $_REQUEST['sender_id']) : '';

        $result = $this->db->get()->result();


        foreach ($result as $d) {

            $account = $this->model->get_user_details($d->username);

            $username= $account != false ? $account->username: $d->username;

            $donedate = strlen($d->donedate) == 10 ? trending_date(sms_time($d->donedate)) : '-';


            $le='A';
            $objWorksheet->getCell($le . $no)->setValue($username);$le++;
            $objWorksheet->getCell($le . $no)->setValue($d->phone);$le++;
            $objWorksheet->getCell($le. $no)->setValue(strtoupper(humanize($d->network)));$le++;
            $objWorksheet->getCell($le . $no)->setValue($d->from);$le++;
            $objWorksheet->getCell($le . $no)->setValue($d->content);$le++;
            $objWorksheet->getCell($le . $no)->setValue($d->client_ref_id);$le++;
            $objWorksheet->getCell($le . $no)->setValue($d->message_status);$le++;
            $objWorksheet->getCell($le . $no)->setValue(date('d-m-Y',$d->created_on));$le++;
            $objWorksheet->getCell($le . $no)->setValue(date('H:i:s',$d->created_on));$le++;
            $objWorksheet->getCell($le . $no)->setValue(date('d-m-Y',$donedate));$le++;
            $objWorksheet->getCell($le . $no)->setValue(date('H:i:s',$donedate));$le++;

            $no++;
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPexcel, 'Excel5');

        $path = 'downloads/';
        if (!is_dir($path)) //create the folder if it's not already exists
        {
            mkdir($path, 0777, TRUE);
        }
        $file_name =  $this->site_name .'-'. $this->page_level2 . ' export ' . time() . '.xls';

        $objWriter->save($dir = $path . $file_name);

        force_download($dir, NULL);

    }

    function users()
    {

        //this is the part where the export is initialised

        $objPHPexcel = PHPExcel_IOFactory::load('excel_templates/template.xlsx');
        $objWorksheet = $objPHPexcel->getActiveSheet(0);
        ///Exports depends on the level


        $header = array('Name', 'Phone', 'Location', 'username', 'Email','Date','Time');

        $L='A';
        $hno=1;
        foreach ($header  as $h){
            $objWorksheet->getCell($L.$hno)->setValue($h);
            $L++;
        }
// Data

        $no = 2;
        $id = $this->db->select()->from('users')->get()->result();
        foreach ($id as $d) {


            $le='A';
            $objWorksheet->getCell($le . $no)->setValue(humanize($d->first_name . ' ' . $d->last_name));$le++;
            $objWorksheet->getCell($le . $no)->setValue(phone($d->phone));$le++;
            $objWorksheet->getCell($le. $no)->setValue($d->city);$le++;
            $objWorksheet->getCell($le . $no)->setValue($d->username);$le++;
            $objWorksheet->getCell($le . $no)->setValue($d->email);$le++;
            $objWorksheet->getCell($le . $no)->setValue(date('d-m-Y',$d->created_on));$le++;
            $objWorksheet->getCell($le . $no)->setValue(date('H:i:s',$d->created_on));$le++;

            $no++;
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPexcel, 'Excel5');

        $path = 'downloads/';
        if (!is_dir($path)) //create the folder if it's not already exists
        {
            mkdir($path, 0777, TRUE);
        }
        $file_name =  $this->site_name .'-'. $this->page_level2 . ' export ' . time() . '.xls';

        $objWriter->save($dir = $path . $file_name);

        force_download($dir, NULL);
    }


    function printing_summary($printing_code=null)
    {

        //this is the part where the export is initialised

        $objPHPexcel = PHPExcel_IOFactory::load('excel_templates/template.xlsx');
        $objWorksheet = $objPHPexcel->getActiveSheet(0);
        ///Exports depends on the level


//        $header = array('Date','Time', 'RequestID', 'Type', 'Applicant','Phone', 'Passport No','Country','Mission');

        $header = array('Names','Mission', 'CardType', 'De', 'Applicant','Phone', 'Passport No','Country','Mission');

        $L='A';
        $hno=1;
        foreach ($header  as $h){
            $objWorksheet->getCell($L.$hno)->setValue($h);
            $L++;
        }
// Data

        $no = 2;
         $this->db->select('a.*,b.title,c.first_name,c.last_name,c.phone,e.country as country_mission,name')->from('applications a');
         $this->db->join('request_type b','a.request_type=b.id');
         $this->db->join('users c','a.user_id=c.id');
        $this->db->join('missions e','a.mission_id=e.id','left');
        $this->db->where(array('printing_code'=>$printing_code));
        $this->db->group_start()->or_where(array('a.request_type'=>1))->or_where(array('a.request_type'=>2))->group_end();
            $id =$this->db->get()->result();


            foreach ($id as $d) {


            $le='A';
            $objWorksheet->getCell($le . $no)->setValue(date('d-m-Y',$d->created_on));$le++;
            $objWorksheet->getCell($le . $no)->setValue(date('H:i:s',$d->created_on));$le++;
            $objWorksheet->getCell($le. $no)->setValue($d->request_code);$le++;
            $objWorksheet->getCell($le. $no)->setValue($d->title);$le++;
            $objWorksheet->getCell($le . $no)->setValue(humanize($d->first_name . ' ' . $d->last_name));$le++;
            $objWorksheet->getCell($le . $no)->setValue(phone($d->phone));$le++;
            $objWorksheet->getCell($le. $no)->setValue($d->passport_no);$le++;
            $objWorksheet->getCell($le . $no)->setValue($this->model->get_country_name($d->country_mission));$le++;
            $objWorksheet->getCell($le . $no)->setValue($d->name);$le++;


            $no++;
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPexcel, 'Excel5');

        $path = 'downloads/';
        if (!is_dir($path)) //create the folder if it's not already exists
        {
            mkdir($path, 0777, TRUE);
        }
        $file_name =  $this->site_name .'-'. $this->page_level2 . ' export ' . time() . '.xls';

        $objWriter->save($dir = $path . $file_name);

        force_download($dir, NULL);
    }



    function data_per_health_facility($from=null,$to=null,$district=null,$facility=null)
    {

        //this is the part where the export is initialised

        $objPHPexcel = PHPExcel_IOFactory::load('excel_templates/template.xlsx');
        $objWorksheet = $objPHPexcel->getActiveSheet(0);
        ///Exports depends on the level

        $indicators = $this->db->select('code,indicator,initial')->from('indicators')->get()->result();


        $header = array('District', 'Facility', 'Level', 'week', 'From', 'to');
        foreach ($indicators as $he):


        array_push($header,$he->initial);
            endforeach;



        $L='A';
        $hno=1;
        foreach ($header  as $h){
            $objWorksheet->getCell($L.$hno)->setValue($h);
            $L++;
        }
// Data

        $no = 2;

        $this->db->select('facility_code,name,level,state')->from('health_facility');
        isset($district) && $district!='empty'?$this->db->where(array('state'=>$district)):'';
        isset($facility) && $facility!='empty'?$this->db->where(array('id'=>$facility)):'';
        $hf =$this->db->get()->result();




        $week_no=week_no(date('Y-m-d',$to));

        $getStartAndEndDate=getStartAndEndDate($week_no,date('Y',$to));

        $from=$getStartAndEndDate[0];
        $to=$getStartAndEndDate[1];



        foreach ($hf as $h) {


            $le='A';
            $objWorksheet->getCell($le . $no)->setValue($h->state);$le++;
            $objWorksheet->getCell($le . $no)->setValue($h->name);$le++;
            $objWorksheet->getCell($le . $no)->setValue($h->level);$le++;
            $objWorksheet->getCell($le . $no)->setValue($week_no);$le++;
            $objWorksheet->getCell($le . $no)->setValue($from);$le++;
            $objWorksheet->getCell($le . $no)->setValue($to);$le++;
            foreach ($indicators as $in):
                $value=$this->indicators_values($in,$h->facility_code,$week_no);
            $objWorksheet->getCell($le . $no)->setValue($value);$le++;
            endforeach;


            $no++;
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPexcel, 'Excel5');

        $path = 'downloads/';
        if (!is_dir($path)) //create the folder if it's not already exists
        {
            mkdir($path, 0777, TRUE);
        }
        $file_name =  'baylor_reporting ' . $this->page_level2 . ' export ' . time() . '.xls';

        $objWriter->save($dir = $path . $file_name);

        force_download($dir, NULL);
    }



}

?>


