<?php
/**
 * Created by PhpStorm.
 * User: DennisTamale
 * Date: 15/09/2017
 * Time: 17:07
 */

class Dashboard_model extends CI_Model
{

    public $out_table;

    function __construct()
    {
        parent::__construct();
        $this->out_table = 'outbox_' . date('dmY');
    }

    function recent_activities()
    {
        return $this->db->select('a.*,b.first_name,b.last_name')->from('logs a')->join('users b', 'a.created_by=b.id', 'left')->limit(7)->order_by('id', 'desc')->get()->result();

    }


    function sum_sent_sms($fdate, $last_day = null,$network=null)
    {

        $ldate = isset($last_day) ? date($last_day) : date('Y-m-d');
        $period = period($fdate, $ldate);


        $sms_count = 0;

        foreach ($period as $dt) {

            $table = 'inbox_' . date('dmY', strtotime($dt->format("Y-m-d")));

            // echo $table;
            $this->auto_create_table->create_inbox_table($table);
            $this->db->select_sum('content_count');
            $this->db
                ->group_start()
                ->where(
                    'status', 'Success'
                )->group_end();

            $this->session->user_type != 1 ? $this->db->where('username', $this->session->id) : '';

            isset($network)?$this->db->where(array('network'=>$network)):'';

            $a = $this->db->from($table)
                ->get()->row();
            $sms_count = $sms_count + intval($a->content_count);


        }

        return $sms_count;

    }

    function networks_sent($fdate,$last_day){


        $networks=array();

        foreach (array('MTN_RW','AIRTEL_RW','TIGO_RW','RWANDACELL') as $network){

           $sent= $this->sum_sent_sms($fdate,$last_day,$network);


            array_push($networks,array('network'=>$network,'sent'=>$sent));


        }

        return json_decode(json_encode($networks));
    }

    function sum_delivered_sms($fdate, $last_day = null)
    {

        $ldate = isset($last_day) ? date($last_day) : date('Y-m-d');
        $period = period($fdate, $ldate);


        $sms_count = 0;

        foreach ($period as $dt) {

            $table = 'inbox_' . date('dmY', strtotime($dt->format("Y-m-d")));

            // echo $table;
            $this->auto_create_table->create_inbox_table($table);
            $this->db->select_sum('content_count');
            $this->db
                ->group_start()
                ->where(
                    'message_status', 'DELIVRD'
                )->group_end();
            $this->session->user_type != 1 ? $this->db->where('username', $this->session->id) : '';
            $a = $this->db->from($table)
                ->get()->row();
            $sms_count = $sms_count + intval($a->content_count);


        }

        return $sms_count;

    }


    function sum_un_delivered_sms($fdate, $last_day = null)
    {

        $ldate = isset($last_day) ? date($last_day) : date('Y-m-d');
        $period = period($fdate, $ldate);


        $sms_count = 0;

        foreach ($period as $dt) {

            $table = 'inbox_' . date('dmY', strtotime($dt->format("Y-m-d")));

            // echo $table;
            $this->auto_create_table->create_inbox_table($table);
            $this->db->select_sum('content_count');
            $this->db
                ->group_start()
                ->where(
                    'message_status !=', 'DELIVRD'
                )->or_where(
                    'message_status is NULL'
                )->group_end();
            $this->session->user_type != 1 ? $this->db->where('username', $this->session->id) : '';
            $a = $this->db->from($table)
                ->get()->row();
            $sms_count = $sms_count + intval($a->content_count);


        }

        return $sms_count;

    }


}