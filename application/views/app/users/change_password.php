
<div class="panel panel-flat">
    <div class="panel-heading ">
        <h5 class="panel-title">Change Password </h5>
        <div class="heading-elements">
            <ul class="icons-list">

                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li>
                <li><a data-action="close"></a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">

<?php echo form_open($this->page_level.'users/change_password/'.$prof->id*date('Y')) ?>
<div class="form-group hidden">
    <label class="control-label">Current Password </label><?php echo form_error('current_password','<label style="color: red;">','</label>') ?>
    <input name="current_password" autocomplete="off" type="password" class="form-control"/>
</div>
<div class="form-group">
    <label class="control-label">New Password </label><?php echo form_error('new_pass','<label style="color: red;">','</label>') ?>
    <input name="new_pass" autocomplete="off" type="password" class="form-control"/>
</div>
<div class="form-group">
    <label class="control-label">Re-type New Password </label><?php echo form_error('rpt_pass','<label style="color: red;">','</label>') ?>
    <input name="rpt_pass" autocomplete="off" type="password" class="form-control"/>
</div>
<div class="margin-top-10">
    <button type="submit" class="btn green-haze">
        Change Password </button>
    <button type="reset" class="btn default">
        Cancel </button>
</div>
<?php echo form_close(); ?>

    </div>
</div>

