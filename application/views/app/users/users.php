
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?= base_url() ?>assets/global/css/components.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="<?= base_url() ?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- END HEAD -->

<!-- Begin: Demo Datatable 1 -->
<div class="panel panel-flat">

    <!--    --><?php //print_r($this->input->post()); ?>
    <?= form_open(); ?>

    <div class="panel-body">
        <div class="table-container">
            <div class="table-actions-wrapper">
                <span> </span>

                <?php echo $this->custom_library->role_exist('create user')? anchor($this->page_level.$this->page_level2.'new',' <i class="fa fa-plus"></i> New user','class="btn btn-sm btn-primary"'):''; ?>



                <select class="table-group-role-input form-control input-sm  input-inline" name="user_role" >
                    <option value="" <?php echo set_select('user_role','',true) ?>>User Role</option>
                    <?php foreach( $this->db->select()->from('user_type')->get()->result() as $u): ?>
                        <option value="<?php echo $u->id  ?>" <?php echo set_select('user_role',$u->id) ?>><?php echo ucwords($u->title) ?></option>

                    <?php endforeach; ?>
                </select>
                <select class="table-group-status-input form-control input-inline input-small input-sm" name="status" >
                    <option value="" <?php echo set_select('status','',true) ?>>Select Status</option>
                    <option value="1" <?php echo set_select('status','1') ?>>Active</option>
                    <option value="2" <?php echo set_select('status','2') ?>>Blocked</option>

                </select>
                <button class="btn btn-xs btn-success table-group-action-submit" type="submit">
                    <i class="fa fa-check"></i> Submit</button>


<!--                <span class="--><?php //echo $this->custom_library->role_exist('Export VHT')?'':'hidde' ?><!--">-->

                    <select <?php echo $this->custom_library->role_exist('Export VHT')?'':'hidde' ?> name="export"  class=" btn btn-warning dropdown-toggle input-inline"  style="    height: 30px;" onchange="this.form.submit()">
                        <option value="">Export</option>
                        <option value="export_pdf">PDF</option>
                        <option value="excel">Excel</option>

                    </select>



<!--                </span>-->



            </div>
            <table class="table datatable-ajax" id="<?= $title ?>">
                <thead>
                <tr>
                    <th width="width: 8px;">
                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                            <input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" />
                            <span></span>
                        </label>
                    </th>
                    <!--                    List Applications: Date, Type, Applicant, Passport, Country, Status, Actions-->
                    <th> Name </th>

                    <th> Country </th>
                    <th> Phone </th>


                    <th> Rate </th>
                    <th> Balance </th>
                    <th> Role </th>

                    <th> Status </th>
                    <th>Action</th>

                </tr>

                </thead>
                <tbody> </tbody>
            </table>
        </div>
    </div>
    <?= form_close() ?>
</div>
<!-- End: Demo Datatable 1 -->


<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?= base_url() ?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?= base_url() ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->

<?php
$this->load->view('ajax/'.$title);
?>
