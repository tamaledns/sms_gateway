<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />


<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="panel panel-flat">
            <div class="panel-heading">
            <div class="panel-title">
                <div class="caption font-green-haze">
                    <i class="icon-user font-green-haze"></i>
                    <?php $user=$this->db->select()->from('users')->where('id',$id)->get()->row(); ?>
                    <span class="caption-subject bold uppercase"> Edit <?php echo $user->first_name.' '.$user->last_name; ?></span>
                </div>
                <div class="actions">
                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                </div>
            </div>
        </div>
            <div class="panel-body form">

                <?php echo form_open('',array('class'=>'form-horizontal')) ?>
<!--                id, full_name, city, password, username, region, country, phone, email, gender, photo, user_type, sub_type, status, verified, created_on, created_by, updated_on, updated_by, id, id-->
                <div class="form-body">
                    <div class="form-group form-md-line-input">
                        <label class="col-md-2 control-label" for="form_control_1">Full Name</label>
                        <div class="col-md-5">
                            <input type="text" required title="Enter Full Name" value="<?php echo set_value('first_name') ?>" name="first_name" class="form-control" placeholder="Enter First Name" />
                            <div class="form-control-focus" style="color: red"><?php echo form_error('first_name') ?> </div>

                        </div>
                        <div class="col-md-5">
                            <input type="text" required title="Enter Last Name" value="<?php echo set_value('last_name') ?>" name="last_name" class="form-control" placeholder="Enter Last Name" />
                            <div class="form-control-focus" style="color: red"><?php echo form_error('last_name') ?> </div>

                        </div>
                    </div>

                    <div class="form-group form-md-line-input">
                        <label class="col-md-2 control-label" for="form_control_1">Date of Birth</label>
                        <div class="col-md-8">
                            <div class="form-group form-md-line-input">
                                <div class="input-group  date date-picker" data-date-format="yyyy-mm-dd">
                                    <input type="text" class="form-control" name="dob" value="<?php echo set_value('dob') ?>">
                                                        <span class="input-group-btn">
                                                            <button class="btn default" type="button">
                                                                <i class="fa fa-calendar"></i>
                                                            </button>
                                                        </span>
                                    <label for="form_control_1"> <?php echo form_error('dob','<span style=" color:red;">','</span>') ?></label>

                                </div>

                            </div>
                        </div>

                    </div>

                    <div class="form-group form-md-line-input">
                        <label class="col-md-2 control-label" for="form_control_1">Email</label>
                        <div class="col-md-10">
                            <input type="email" class="form-control" required name="email" value="<?php echo set_value('email') ?>" id="form_control_1" placeholder="User Email address">
                            <div class="form-control-focus" style="color: red"><?php echo form_error('email') ?> </div>
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label class="col-md-2 control-label" for="form_control_1">Zip Code / Phone Number</label>
                        <div class="col-md-2">
                            <select class="form-control" name="zip_code"  >
                                <option value="" <?php echo set_select('zip_code', '', TRUE); ?> >Select Zip Code</option>

                                <?php foreach($this->db->select('dialing_code,a2_iso')->from('country')->order_by('a2_iso','asc')->get()->result() as $cat){ ?>
                                    <option value="<?php echo $cat->dialing_code  ?>"  <?php echo set_select('zip_code', $cat->dialing_code); ?> ><?php echo $cat->a2_iso.'('.$cat->dialing_code.')' ?></option>
                                <?php } ?>

                            </select>
                            <label for="form_control_1"> <?php echo form_error('zip_code','<span style=" color:red;">','</span>') ?></label>
                            <span class="help-block">Choose the Zip code for your country</span>
                        </div>
                        <div class="col-md-8">
                            <input type="text" class="form-control" required name="phone" value="<?php echo set_value('phone') ?>" id="form_control_1" placeholder="User Phone Number(e.g 256xxxxxxxx)">
                            <div class="form-control-focus" style="color: red"><?php echo form_error('phone') ?> </div>
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="form-group  form-md-radios">
                        <label class="col-md-2 control-label">Gender</label>
                        <div class="md-radio-inline  col-md-10">
                            <div class="md-radio">
                                <input type="radio" id="radio6" name="gender" value="M" <?php echo set_radio('gender','M'); ?> class="md-radiobtn">
                                <label for="radio6">
                                    <span></span>
                                    <span class="check"></span>
                                    <span class="box"></span> Male </label>
                            </div>
                            <div class="md-radio">
                                <input type="radio" id="radio7" name="gender" value="F" <?php echo set_radio('gender','F'); ?> class="md-radiobtn">
                                <label for="radio7">
                                    <span></span>
                                    <span class="check"></span>
                                    <span class="box"></span> Female </label>
                            </div>
                            <label class="display-block">Gender: <?php echo form_error('gender','<span style=" color:red;">','</span>') ?></label>
                        </div>



                    <div class="form-group form-md-line-input">
                        <label class="col-md-2 control-label" for="form_control_1">Country / City</label>
                        <div class="col-md-5">
                            <select class="form-control" name="country"  >
                                <option value="" <?php echo set_select('country', ''); ?> >Select Country</option>

                                <?php foreach($this->db->select('country,dialing_code,a2_iso')->from('country')->get()->result() as $cat){ ?>

                                    <option value="<?php echo $cat->a2_iso  ?>"  <?php echo set_select('country', $cat->a2_iso,$cat->a2_iso=='UG'?true:''); ?> ><?php echo $cat->country ?></option>
                                <?php } ?>

                            </select>
                            <label for="form_control_1"> <?php echo form_error('country','<span style=" color:red;">','</span>') ?></label>
                            <span class="help-block">Choose the Country</span>
                        </div>

                        <div class="col-md-5">

                            <select class="form-control" name="city" hidden >
                                <option value="" <?php echo set_select('city', '', TRUE); ?> >Select City</option>

                                <?php foreach($this->db->select('state,title')->from('state')->get()->result() as $cat){ ?>
                                    <option value="<?php echo $cat->state ?>"  <?php echo set_select('city', $cat->state); ?> ><?php echo $cat->title ?></option>
                                <?php } ?>

                            </select>
                            <label for="form_control_1"> <?php echo form_error('state','<span style=" color:red;">','</span>') ?></label>
                            <span class="help-block">Choose the City</span>
                        </div>
                    </div>

                        <?php if($this->session->userdata('user_type')!='2'){ ?>

                    <div class="form-group form-md-line-input">
                        <label class="col-md-2 control-label" for="form_control_1">Role</label>
                        <div class="col-md-4">
                            <select class="form-control" name="role"  >
                                <option value="" <?php echo set_select('role', '', TRUE); ?> >Select User Role</option>
                                <?php foreach($this->db->select('id,title')->from('user_type')->order_by('id','asc')->get()->result() as $role): ?>
                                    <option value="<?php echo $role->id ?>" <?php echo set_select('role', $role->id); ?> ><?php echo $role->title ?></option>
                                <?php  endforeach; ?>


                            </select>
                            <label for="form_control_1"> <?php echo form_error('country','<span style=" color:red;">','</span>') ?></label>
                            <span class="help-block">Choose the Country</span>
                        </div>

                        <label class="col-md-2 control-label">Access Control</label>
                        <div class="md-radio-inline  col-md-4">
                            <div class="md-radio">
                                <input type="radio" id="c" name="access" value="c" <?php echo set_radio('access','c'); ?> class="md-radiobtn">
                                <label for="c">
                                    <span></span>
                                    <span class="check"></span>
                                    <span class="box"></span> Country </label>
                            </div>
                            <div class="md-radio">
                                <input type="radio" id="b" name="access" value="b" <?php echo set_radio('access','b'); ?> class="md-radiobtn">
                                <label for="b">
                                    <span></span>
                                    <span class="check"></span>
                                    <span class="box"></span> Branch </label>
                            </div>

                        </div>
                    </div>



                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-2 col-md-10">
                            <button type="reset" class="btn default"> <i class="fa fa-remove"></i> Cancel</button>
                            <button type="submit" class="btn green-jungle"><i class="fa fa-save"></i> Save</button>
                        </div>
                    </div>
                </div>
                    <?php } ?>
                <?php echo form_close(); ?>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->

    </div>


</div>

    <!-- BEGIN CORE PLUGINS -->
    <script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="<?php echo base_url() ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="<?php echo base_url() ?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
