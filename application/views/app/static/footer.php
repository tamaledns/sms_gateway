

<div class="footer text-muted">
    &copy; <?= date('Y') ?>. <a href="<?= base_url() ?>"><?php echo $this->site_options->title('site_name') ?></a> by <a href="#" target="_blank">Company</a>
</div>
<!-- /footer -->

</div>
<!-- /content area -->

</div>
<!-- /main content -->

</div>
<!-- /page content -->

</div>
<!-- /page container -->

</body>
</html>
