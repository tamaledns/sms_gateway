<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="Dennis Tamale(tamaledns@gmail.com)" name="author"/>
    <title><?php echo isset($title)? humanize($title):'' ?> | <?php echo $this->site_options->title('site_name') ?></title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="<?= base_url() ?>assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="<?= base_url() ?>assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
    <link href="<?= base_url() ?>assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="<?= base_url() ?>assets/css/core.css" rel="stylesheet" type="text/css">
    <link href="<?= base_url() ?>assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="<?= base_url() ?>assets/css/colors.css" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" href="<?php echo base_url($this->site_options->title('site_logo')) ?>" />
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/loaders/blockui.min.js"></script>


    <!--    <script type="text/javascript" src="--><?//= base_url() ?><!--assets/js/plugins/notifications/bootbox.min.js"></script>-->
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/notifications/sweet_alert.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->

    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/forms/selects/select2.min.js"></script>


    <script type="text/javascript" src="<?= base_url() ?>assets/js/pages/datatables_extension_buttons_init.js"></script>

    <?php

//    if($this->uri->segment(2)!=''){ ?>

<!--    these are the assets for the date picker-->
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/notifications/jgrowl.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/ui/moment/moment.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/pickers/daterangepicker.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/pickers/anytime.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/pickers/pickadate/picker.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/pickers/pickadate/picker.date.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/pickers/pickadate/picker.time.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/pickers/pickadate/legacy.js"></script>

<!--    --><?php //} ?>

<!--    these are the assets for the forms-->
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/forms/styling/uniform.min.js"></script>


    <script type="text/javascript" src="<?= base_url() ?>assets/js/core/app.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/pages/components_modals.js"></script>


        <script type="text/javascript" src="<?= base_url() ?>assets/js/pages/form_select2.js"></script>



    <?php  // if($this->uri->segment(2)!=''){ ?>

        <script type="text/javascript" src="<?= base_url() ?>assets/js/pages/picker_date.js"></script>

    <?php //} ?>


    <script type="text/javascript" src="<?= base_url() ?>assets/js/pages/datatables_basic.js"></script>


    <script type="text/javascript" src="<?= base_url() ?>assets/js/pages/form_layouts.js"></script>




    <!-- /theme JS files -->



</head>

<body class="<?= $this->uri->segment(2)=='messaging'?'sidebar-xs':''; ?>  sidebar-x has-detached-left">
<!-- Main navbar -->
<div class="navbar navbar-inverse" style="background: <?//= $this->site_options->title('site_color_code') ?>;">
    <div class="navbar-header">
        <a class="navbar-brand" href="<?php echo base_url()?>"><img src="<?= base_url($this->site_options->title('site_logo')) ?>" alt="<?php echo $this->site_options->title('site_name') ?>"></a>

        <ul class="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <?= $this->uri->segment(2)=='messaging'?'<li><a class="sidebar-control sidebar-main-hide hidden-xs"><i class="icon-lan3"></i></a></li>':''?>
<?= $this->uri->segment(2)=='messaging'?'':'<li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>'?>

            <li class="dropdown">
                <a href="#" class="dropdown-toggle hidden" data-toggle="dropdown">
                    <i class="icon-git-compare"></i>
                    <span class="visible-xs-inline-block position-right">Git updates</span>
                    <span class="badge bg-warning-400">9</span>
                </a>

                <div class="dropdown-menu dropdown-content">
                    <div class="dropdown-content-heading">
                        Git updates
                        <ul class="icons-list">
                            <li><a href="#"><i class="icon-sync"></i></a></li>
                        </ul>
                    </div>

                    <ul class="media-list dropdown-content-body width-350">
                        <li class="media">
                            <div class="media-left">
                                <a href="#" class="btn border-primary text-primary btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-pull-request"></i></a>
                            </div>

                            <div class="media-body">
                                Drop the IE <a href="#">specific hacks</a> for temporal inputs
                                <div class="media-annotation">4 minutes ago</div>
                            </div>
                        </li>

                    </ul>

                    <div class="dropdown-content-footer">
                        <a href="#" data-popup="tooltip" title="All activity"><i class="icon-menu display-block"></i></a>
                    </div>
                </div>
            </li>
        </ul>

        <p class="navbar-text">
            <span class="label bg-success">Balance : <?= $this->model->account_balance($this->session->id); ?></span>
        </p>

        <div class="navbar-right">
            <ul class="nav navbar-nav">


                <li class="dropdown hidden">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-bubbles4"></i>
                        <span class="visible-xs-inline-block position-right">Messages</span>
                        <span class="badge bg-warning-400">2</span>
                    </a>

                    <div class="dropdown-menu dropdown-content width-350">
                        <div class="dropdown-content-heading">
                            Messages
                            <ul class="icons-list">
                                <li><a href="#"><i class="icon-compose"></i></a></li>
                            </ul>
                        </div>

                        <ul class="media-list dropdown-content-body">
                            <li class="media">
                                <div class="media-left">
                                    <img src="<?= base_url() ?>assets/images/placeholder.jpg" class="img-circle img-sm" alt="">
                                    <span class="badge bg-danger-400 media-badge">5</span>
                                </div>

                                <div class="media-body">
                                    <a href="#" class="media-heading">
                                        <span class="text-semibold">James Alexander</span>
                                        <span class="media-annotation pull-right">04:58</span>
                                    </a>

                                    <span class="text-muted">who knows, maybe that would be the best thing for me...</span>
                                </div>
                            </li>


                        </ul>

                        <div class="dropdown-content-footer">
                            <a href="#" data-popup="tooltip" title="All messages"><i class="icon-menu display-block"></i></a>
                        </div>
                    </div>
                </li>

                <li class="dropdown dropdown-user">
                    <a class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?php echo base_url($this->session->userdata('photo')) ?>" alt="">
                        <span><?php echo ucwords($this->session->userdata('first_name')); ?> </span>
                        <i class="caret"></i>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li>
                            <?php echo anchor($this->page_level.'profile','<i class="icon-user-plus"></i> Account settings'); ?>
                        </li>
                        <li class="hidden"><a href="#"><i class="icon-coins"></i> My balance</a></li>
                        <li class="hidden"><a href="#"><span class="badge bg-blue pull-right">58</span> <i class="icon-comment-discussion"></i> Messages</a></li>
                        <li class="divider"></li>

                        <li>
                            <?php echo anchor($this->page_level.'logout','<i class="icon-switch2"></i> Logout'); ?>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- /main navbar -->




<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">



        <?php $this->load->view('app/static/main_sidebar'); ?>

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            <div class="page-header">
                <div class="page-header-content" style="margin-bottom: 15px;">


                </div>

                <div class="breadcrumb-line breadcrumb-line-component">
                    <ul class="breadcrumb">
                        <li>
                            <?php echo anchor($this->uri->segment(1),'<i class="icon-home2 position-left"></i> Home') ?>
                           </li>
                        <li><?php echo isset($title)?( strlen($title)>0? anchor($this->page_level.$title,humanize($title)):''):''; ?></li>
                        <li class="active"> <?php  echo isset($subtitle)?( strlen($subtitle)>0?humanize($subtitle):''):''; ?></li>


                    </ul>
                    <?php echo form_open($this->page_level.$title.($title=='reports'?'/dashboard':'')) ?>
                    <ul class="breadcrumb-elements">

                        <li class="<?= $this->uri->segment(2)==''||$this->uri->segment(2)=='dashboard'||$this->uri->segment(3)=='dashboard'?'':'hidden'; ?>" style="padding: 4px;">


                            <div class="input-group">
                                <span class="input-group-addon"><i class="icon-calendar22"></i></span>


                                <input style="width: 175px;" name="date_range" readonly value=""  type="text" class="form-control daterange-basi daterange-predefined form-filter input-x" required="required"
                                       data-popup="tooltip" title="" data-placement="top" data-original-title="Date of "
                                />

                            </div>



                            </li>

                        <li class="<?= $this->uri->segment(2)==''||$this->uri->segment(2)=='dashboard'||$this->uri->segment(3)=='dashboard'?'':'hidden'; ?>" style="padding: 4px;">

                            <button style="margin-top: 4px;" class="btn green btn-default filter-submit margin-bottom">
                                <i class="fa fa-search"></i> Filter</button>

                        </li>



                    </ul>
                    <?= form_close(); ?>
                </div>
            </div>
            <!-- /page header -->


            <!-- Content area -->
            <div class="content">

