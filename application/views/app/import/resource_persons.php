<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css"/>

<link href="<?php echo base_url() ?>assets/admin/pages/css/profile.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>assets/admin/pages/css/tasks.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->
<?php $prof=$this->db->select()->from('users')->where('id',$this->session->userdata('id'))->get()->result();
$prof=$prof[0];
?>





<div class="row">

    <div class="col-md-12">
        <!-- BEGIN TAB PORTLET-->
        <div class="portlet light ">

            <div class="portlet-body">

                <div class="tabbable tabbable-tabdrop">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#resource_persons" data-toggle="tab">Resource Persons</a>
                        </li>
                        <li>
                            <a href="#add_new" data-toggle="tab">Uploads</a>
                        </li>



                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="resource_persons">
                            <p> <?php $this->load->view($this->page_level.'resources/resource_persons') ?></p>
                        </div>
                        <div class="tab-pane" id="add_new">
                            <p>
                            <div class="portlet light form-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-cloud-upload font-green"></i>
                                        <span class="caption-subject font-green sbold uppercase">Upload File</span>
                                    </div>
                                    <div class="actions ">
                                        <!--                    <input  type="checkbox" class="make-switch " checked data-on="success" data-on-color="success" data-off-color="warning" data-size="small">-->

                                        <div class="btn-group  hidden-print">


                                            <a class="btn green  btn-outline " href="<?php echo base_url('uploads/excel_templates/resource_persons.xlsx') ?>" download ><i class=" fa fa-cloud-download" ></i> Download Template </a>

                                        </div>

                                    </div>

                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <?php echo form_open_multipart('',' class="form-horizontal form-bordered"')?>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">File
                                            <?php if( isset($error)){?>
                                                <span class="font-red-mint" >
                                        <?php echo  $error; ?>

                                    </span>
                                            <?php } echo form_error('file_upload','<label style="color:red;">','</label>') ?></label>
                                        <div class="col-m
    </div>
                    <div class="col-md-3">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="input-group input-large">
                                                <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                                    <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                                    <span class="fileinput-filename"> </span>
                                                </div>
                                                            <span class="input-group-addon btn default btn-file">
                                                                <span class="fileinput-new"> Select file </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <input type="file" name="file_upload"> </span>
                                                <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                            </div>
                                        </div>
                                    </div>

                                </div>




                                <?php echo form_error('pa','<label style="color:red;">','</label>') ?>
                                <input type="hidden" name="pa"  placeholder="What is your current password ?"  class="form-control"/>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">

                                            <button type="submit" class="btn green-jungle">Upload <i class="fa fa-upload"></i></button>
                                            <a href="#" class="btn default">
                                                Cancel </a>
                                        </div>


                                    </div>
                                </div>
                                <?php echo form_close() ?>


                                <!-- END FORM-->
                            </div>
                            </p>

                            <?php $this->load->view($this->page_level.'import/file_uploads') ?>

                        </div>


                    </div>
                </div>
                <p> &nbsp; </p>

            </div>
        </div>
        <!-- END TAB PORTLET-->

    </div>

    <?php //$this->load->view($this->page_level.'resources/resource_persons'); ?>


    </div>
</div>





<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>

<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
