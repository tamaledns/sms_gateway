<div class="row">
    <div class="col-md-12">

        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green-haze">
                    <i class="fa fa-money font-green-haze"></i>
                    <span class="caption-subject bold uppercase">Add Commission</span>
                </div>

                <div class="actions">
                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                    <!--                    --><?php //echo anchor($this->page_level.$this->page_level2,' <i class="fa fa-users"></i> Users','class="btn btn-circle btn-warning btn-sm"'); ?>
                </div>
            </div>
            <div class="portlet-body form">
                <?php echo form_open('') ?>
                <div class="form-body">

                    <?php
                    //'a.id,a.from_amount,a.to_amount,a.percentage,b.country'
                    $comm=$this->db->select('a.*,b.country as title')->from('commission a')->join('selected_countries b','a.country=b.a2_iso')->where('id',$id)->get()->row(); ?>

                    <div class="row">




                        <div class="col-md-2">
                            <?php $co=$this->db->select()->from('selected_countries')->where('a2_iso != ',$comm->country)->get()->result() ?>
                            <div class="form-group form-md-line-input has-success">
                                <select class="form-control" name="country"  >
                                    <option value="<?php echo $comm->country ?>" <?php echo set_select('country', $comm->country, TRUE); ?> ><?php echo $comm->title ?></option>

                                    <?php foreach($co as $to){ ?>
                                        <option value="<?php echo $to->a2_iso  ?>"  <?php echo set_select('country', $to->a2_iso); ?> ><?php echo $to->country ?></option>
                                    <?php } ?>

                                </select>
                                <label for="form_control_1">Country </label><?php echo form_error('country','<row style=" color:red;">','</row>') ?>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group form-md-line-input has-success">
                                <select class="form-control" name="type"  >
                                    <option value="<?php echo $comm->type ?>" <?php echo set_select('type', '', TRUE); ?> ><?php echo $comm->type ?></option>
                                    <option value="local" <?php echo set_select('type', 'local'); ?> >Local</option>
                                    <option value="foreign" <?php echo set_select('type', 'foreign'); ?> >Foreign</option>
                                </select>
                                <label for="form_control_1">Type </label><?php echo form_error('type','<row style=" color:red;">','</row>') ?>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group form-md-line-input has-success">
                                <select class="form-control" name="units"  >
                                    <option value="<?php echo $comm->units ?>" <?php echo set_select('units', $comm->units, TRUE); ?> ><?php echo $comm->units ?></option>
                                    <option value="amount" <?php echo set_select('units', 'amount'); ?> >Amount</option>
                                    <option value="percent" <?php echo set_select('units', 'percent'); ?> >Percentage</option>
                                </select>
                                <label for="form_control_1">Units </label><?php echo form_error('units','<row style=" color:red;">','</row>') ?>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group form-md-line-input has-success">
                                <input type="text" class="form-control" name="unit_value" value="<?php echo $comm->unit_value ?>" placeholder="Unit value">
                                <label for="form_control_1">Unit Value  <?php echo form_error('unit_value','<span style=" color:red;">','</span>') ?></label>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group form-md-line-input has-success">
                                <input type="text"  class="form-control" name="min" value="<?php echo $comm->min; ?>" placeholder="Min">
                                <label for="form_control_1">Min  <?php echo form_error('min','<span style=" color:red;">','</span>') ?></label>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group form-md-line-input has-success">
                                <input type="text" class="form-control" name="max" value="<?php echo $comm->max ?>" placeholder="Max">
                                <label for="form_control_1">Max  <?php echo form_error('max','<span style=" color:red;">','</span>') ?></label>
                            </div>
                        </div>




                    </div>

                </div>
                <div class="form-actions">
                    <button type="submit" class="btn blue">Submit</button>
                    <button type="button" class="btn default">Cancel</button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->

    </div>

</div>