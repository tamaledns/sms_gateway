<?php $messages=$this->db->select()->from('inbox')->order_by('id','desc')->limit(100)->get()->result() ?>




<?php

$status_list = array(
    array("success" => "Pending"),
    array("info" => "Closed"),
    array("danger" => "On Hold"),
    array("warning" => "Fraud"),
    array("primary" => "Fraud"),
    array("pink" => "Fraud"),
    array("violet" => "Fraud")
);

?>


<script type="text/javascript" src="<?= base_url() ?>assets/js/core/libraries/jasny_bootstrap.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/forms/styling/uniform.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/pages/mail_list.js"></script>
<!-- /theme JS files -->


<!-- Multiple lines -->
<div class="panel panel-white">
    <div class="panel-heading hidden">
        <h6 class="panel-title">My Inbox (multiple lines)</h6>

        <div class="heading-elements not-collapsible">
            <span class="label bg-blue heading-text">259 new today</span>
        </div>
    </div>

    <div class="panel-toolbar panel-toolbar-inbox">
        <div class="navbar navbar-default">
            <ul class="nav navbar-nav visible-xs-block no-border">
                <li>
                    <a class="text-center collapsed" data-toggle="collapse" data-target="#inbox-toolbar-toggle-multiple">
                        <i class="icon-circle-down2"></i>
                    </a>
                </li>
            </ul>

            <div class="navbar-collapse collapse" id="inbox-toolbar-toggle-multiple">
                <div class="btn-group navbar-btn">
                    <button type="button" class="btn btn-default btn-icon btn-checkbox-all">
                        <input type="checkbox" class="styled">
                    </button>

                    <button type="button" class="btn btn-default btn-icon dropdown-toggle" data-toggle="dropdown">
                        <span class="caret"></span>
                    </button>

                    <ul class="dropdown-menu">
                        <li><a href="#">Select all</a></li>
                        <li><a href="#">Select read</a></li>
                        <li><a href="#">Select unread</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Clear selection</a></li>
                    </ul>
                </div>

                <div class="btn-group navbar-btn">
                    <button type="button" class="btn btn-default"><i class="icon-pencil7"></i> <span class="hidden-xs position-right">Compose</span></button>
                    <button type="button" class="btn btn-default"><i class="icon-bin"></i> <span class="hidden-xs position-right">Delete</span></button>
                    <button type="button" class="btn btn-default"><i class="icon-spam"></i> <span class="hidden-xs position-right">Spam</span></button>
                </div>

                <div class="navbar-right">
                    <p class="navbar-text"><span class="text-semibold">1-50</span> of <span class="text-semibold">528</span></p>

                    <div class="btn-group navbar-left navbar-btn">
                        <button type="button" class="btn btn-default btn-icon disabled"><i class="icon-arrow-left12"></i></button>
                        <button type="button" class="btn btn-default btn-icon"><i class="icon-arrow-right13"></i></button>
                    </div>

                    <div class="btn-group navbar-btn">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-cog3"></i>
                            <span class="caret"></span>
                        </button>

                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li><a href="#">One more line</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-inbox">
            <tbody data-link="row" class="rowlink">

            <?php foreach ($messages as $m){

                $status = $status_list[rand(0, 6)];
                ?>

            <tr class="unread">
                <td class="table-inbox-checkbox rowlink-skip">
                    <input type="checkbox" class="styled">
                </td>
                <td class="table-inbox-star rowlink-skip">
                    <a href="<?= base_url($this->page_level.$this->page_level2.'read/'.$m->id*date('Y').'/inbox') ?>">
                        <i class="icon-star-empty3 text-muted"></i>
                    </a>

                </td>
                <td class="table-inbox-image">
											<span class="btn bg-<?= key($status) ?>-400 btn-rounded btn-icon btn-xs">
												<span class="letter-icon"></span>
											</span>
                </td>
                <td class="table-inbox-name">
                    <a href="#">
                        <?php $con=$this->db->select()->from('users')->where(array('phone'=>$m->phone))->get()->row();
                        $phone=!empty($con)?$con->first_name.' '.$con->last_name:$m->phone;

                        ?>

                        <div class="letter-icon-title text-default"><?= $phone ?></div>
                        <span class="table-inbox-preview"><?= $phone=!empty($con)?$m->phone:''; ?></span>
                    </a>
                </td>
                <td class="table-inbox-message">
                    <div class="table-inbox-subject"><span class="label bg-success position-left hidden">Promo</span> <?= humanize($m->subject) ?></div>
                    <span class="table-inbox-preview"><?= $m->message ?></span>
                </td>
                <td class="table-inbox-attachment">
                    <i class="icon-attachment text-muted"></i>
                </td>
                <td class="table-inbox-time" style="white-space: nowrap;">
                    <?= trending_date($m->created_on) ?>
                </td>
            </tr>

            <?php } ?>

            </tbody>
        </table>
    </div>
</div>
<!-- /multiple lines -->