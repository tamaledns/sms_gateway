<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">

<?php //print_r( $this->site_options->title()); ?>
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="panel panel-flat">
            <div class="panel-heading">
            <div class="panel-title">
                <div class="caption font-dark">
                    <i class="icon-globe font-dark"></i>
                    <span class="caption-subject bold uppercase"><?php echo humanize($title); ?></span>

<!--                    <div class="btn btn-sm green"><i class="fa fa-plus"></i> Add New</div>-->
                </div>
                <div class="actions pull-left">

                    <?php //echo anchor($this->page_level.$this->page_level2.'new_country',' <i class="fa fa-plus"></i> New Country','class="btn green-jungle btn-sm"'); ?>
                </div>
                <div class="tools"> </div>
            </div>
        </div>
            <div class="panle-body">
                <table class="table datatable-basic" id="sample_">

                    <thead>
                    <tr>
                        <th width="5">#</th>
                        <th> Code </th>
                        <th> Title</th>
                        <th width="70">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=1;
                    foreach($this->db->select()->from('site_options')->limit(28)->get()->result() as $c): ?>
                        <tr>
                            <td><?php echo $no; ?></td>
                            <td style="white-space: nowrap;"> <?php echo humanize($c->option_name) ?> </td>
                            <td>

                        <?php if($c->option_name=='site_color_code'){

                            echo '<span style="color:#fff; background-color:'.$c->option_value.';">'.$c->option_value.'</span>';

                                }elseif($c->option_name=='site_logo'){

                            echo img(array(
                                'src'=>$c->option_value,
                                'width'=>80,
                                'height'=>80
                            ));

                        } else{
                                    echo $c->option_value;
                                } ?>



                            </td>

                            <td>

                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li>

                                                <?php echo anchor($this->page_level.$this->page_level2.'edit/'.$c->id*date('Y'),'<i class="fa fa-edit"></i>Edit'); ?>

                                            </li>
                                        </ul>
                                    </li>
                                </ul>

                </td>
                        </tr>
                        <?php
                        $no++;
                    endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>
<!-- END PAGE BASE CONTENT -->