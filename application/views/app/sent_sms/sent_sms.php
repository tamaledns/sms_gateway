
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />


<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable ">
            <div class="portlet-title hidde " style="    padding: 0px 20px 0px;">


                <div class="caption font-dark" style="    padding: 19px 0;">
                    <i class="icon-home font-dark"></i>
                    <span class="caption-subject bold uppercase"><?php echo humanize($title) ?></span>

                    <?php //echo anchor($this->page_level.$this->page_level2.'new',' <i class="fa fa-plus"></i> Add New ','class="btn btn-sm green-jungle"'); ?>
                </div>


                <div class="tools">
                <div class="dt-button buttons-print btn dark btn-outline"><i class="fa fa-export"></i>Print</div>
                <div class="dt-button buttons-print btn green btn-outline"><i class="fa fa-export"></i>PDF</div>
                <div class="dt-button buttons-print btn purple btn-outline"><i class="fa fa-export"></i>CSV</div>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <div class="table-actions-wrapper hidden-print">

                        <div class="form-inline">
                            <div class="form-group">




                                    <div class="input-group input-large date-picker input-daterange" data-date="<?php echo date('Y-m-d') ?>" data-date-format="yyyy-mm-dd">
                                        <span class="input-group-addon">From </span>
                                        <input type="text" class="form-control table-group-from-input input-sm" name="from" value="<?php echo set_value('from') ?>">
                                        <span class="input-group-addon">to </span>
                                        <input type="text" class="form-control table-group-to-input input-sm" name="to" value="<?php echo set_value('to')?>">
                                    </div>



<!--                                <select  class="table-group-sector-input form-control input-inline input-sm" style="width: 100px;">-->
<!--                                    <option value="">sector...</option>-->
<!--                                    --><?php //foreach($this->db->select('id,sector')->from('sectors')->get()->result() as $sc): ?>
<!--                                        <option value="--><?php //echo $sc->id ?><!--">--><?php //echo $sc->sector ?><!--</option>-->
<!--                                    --><?php //endforeach; ?>
<!--                                </select>-->
<!---->
<!--                                <select  class="table-group-publication-input form-control input-inline input-sm" style="width: 130px;">-->
<!--                                    <option value="">Publication Type...</option>-->
<!--                                    --><?php //foreach($this->db->select('id,title')->from('publication_types')->get()->result() as $p): ?>
<!--                                        <option value="--><?php //echo $p->id ?><!--">--><?php //echo $p->title ?><!--</option>-->
<!--                                    --><?php //endforeach; ?>
<!---->
<!--                                </select>-->

                                    <button class="btn btn-sm green table-group-action-submit">
                                        <i class="fa fa-sliders"></i> Apply</button>



                            </div>
                        </div>


                    </div>
                    <table class="table table-striped table-bordered table-hover table-checkable" id="<?php echo $title ?>">
                        <thead>
                        <tr role="row" class="heading">
<!--                            <th width="1%">-->
<!--                                <input type="checkbox" class="group-checkable"> </th>-->
                            <th width="1%"> # </th>
                            <th width="20%"> smsdata </th>
                            <th width="5%"> Sender </th>
                            <th width="5%"> Receiver </th>
                            <th width="5%"> Date </th>
                            <th width="10%"> smsc_id </th>
                            <th width="5%"> Service </th>
                            <th width="2%"> Actions </th>
                        </tr>
                        <tr role="row" class="filter" hidden>

<!--                            <td><input type="text" class="form-control form-filter input-sm" name="title"> </td>-->
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="title">

                            </td>

                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="content"> </td>
                            <td>
                                <select name="sectors" class="form-control form-filter input-sm">
                                    <option value="">Select...</option>
                                    <?php foreach ($this->db->select()->from('sectors')->get()->result() as $pb_type): ?>


                                        <option value="<?php echo $pb_type->id ?>"><?php echo humanize($pb_type->sector); ?></option>
                                    <?php endforeach; ?>
                                </select></td>
                            <td>
                                <select name="publication_type" class="form-control form-filter input-sm">
                                    <option value="">Select...</option>
                                    <?php foreach ($this->db->select()->from('publication_types')->get()->result() as $pb_type): ?>


                                    <option value="<?php echo $pb_type->id ?>"><?php echo humanize($pb_type->title); ?></option>
                                   <?php endforeach; ?>
                                </select>

                            <td>

                                <div class="input-group date date-picker margin-bottom-5" data-date-format="dd-mm-yyyy">
                                    <input type="text" class="form-control form-filter input-sm" readonly name="date" placeholder="From">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-sm default" type="button">
                                                                    <i class="fa fa-calendar"></i>
                                                                </button>
                                                            </span>
                                </div>

                            </td>
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="author">
                            </td>
                            <td>
                                <select name="status" class="form-control form-filter input-sm">
                                    <option value="">Select...</option>
                                    <option value="pending">Pending</option>
                                    <option value="evaluated">Evaluated</option>
                                    <option value="published">Published</option>
                                    <option value="canceled">Canceled</option>
                                </select>
                            </td>
                            <td>
                                <div class="margin-bottom-5">
                                    <button class="btn btn-sm green btn-outline filter-submit margin-bottom">
                                        <i class="fa fa-search"></i> Search</button>
                                </div>
                                <button class="hidden btn btn-sm red btn-outline filter-cancel">
                                    <i class="fa fa-times"></i> Reset</button>
                            </td>
                        </tr>
                        </thead>
                        <tbody> </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>

<?php
$this->load->view('ajax/'.$title);
?>

<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

