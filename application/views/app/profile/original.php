

<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css"/>
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo base_url() ?>assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->

<link href="<?php echo base_url() ?>assets/admin/pages/css/tasks.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->
<?php $data['prof']=$prof=$this->db->select()->from('users')->where('id',$this->session->userdata('id'))->get()->row();
?>
<div class="row">

    <!-- BEGIN PROFILE SIDEBAR -->
    <div class="profile-sidebar col-md-3">
        <!-- PORTLET MAIN -->
        <div class="panel panel-flat">
            <!-- SIDEBAR USERPIC -->
            <div class="profile-userpic">
                <img src="<?php echo strlen($prof->photo)>0?base_url().$prof->photo:base_url().'assets/profile_placeholder.png' ?>" class="img-responsive" alt="">
            </div>
            <!-- END SIDEBAR USERPIC -->
            <!-- SIDEBAR USER TITLE -->


            <div class="panel-heading">
                <div class="profile-usertitle-name">
                    <?php echo ucwords($prof->first_name.' '.$prof->last_name); ?>
                </div>

            </div>
            <!-- END SIDEBAR USER TITLE -->


        </div>
        <!-- END PORTLET MAIN -->

    </div>
    <!-- END BEGIN PROFILE SIDEBAR -->
    <!-- BEGIN PROFILE CONTENT -->
    <div class="profile-content col-md-9">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-flat">



                    <div class="panel-heading">
                        <div class="panel-title tabbable-line">
                            <div class="caption caption-md">



                                <div class="panel-headings">

                                    <i class="icon-globe theme-font hide"></i>
                                    <span class="caption-subject font-blue-madison bold uppercase" style="font-size: larger;"> <?php echo ucwords($prof->first_name.' '.$prof->last_name); ?> Profile Account &nbsp;</span>

                                    <div class="heading-elements">

                                        <!--                                        --><?php //echo anchor($this->page_level.$this->page_level2.'new',' <i class="fa fa-plus"></i> New','class="btn btn-primary btn-xs"'); ?>
                                        <!--                                        --><?php //echo anchor($this->page_level.$this->page_level2,' <i class="fa fa-users"></i> Users','class="btn btn-warning btn-xs"'); ?>

                                        <?php if($this->session->user_type==7){ ?>

                                            <?php //echo anchor($this->page_level.'arrival_departure/add_dependant/'.$prof->id*date('Y'),'  <i class="icon-user-plus"></i> Add Dependant','class="btn bg-violet btn-xs"') ?>

                                        <?php }  ?>

                                    </div>
                                </div>
                            </div>


                        </div>


                    </div>




                    <div class="panel-body">

                        <ul class="nav nav-tabs">
                            <li class="<?php echo $subtitle=='user_profile'||$subtitle=='info'?'active':''; ?>">
                                <a href="#tab_1_1" data-toggle="tab">Account Info</a>
                            </li>

                            <li class="<?php echo $subtitle=='change_avatar'?'active':''; ?>">
                                <a href="#tab_1_2" data-toggle="tab">Change Avatar</a>
                            </li>
                            <li <?php echo $subtitle=='change_password'?'active':''; ?>>
                                <a href="#tab_1_3" data-toggle="tab">Change Password</a>
                            </li>

                        </ul>

                        <div class="tab-content">
                            <!-- PERSONAL INFO TAB -->
                            <div class="tab-pane fade in <?php echo $subtitle=='user_profile'||$subtitle=='info'?'active':''; ?>" id="tab_1_1">

                                <?php echo form_open($this->uri->slash_segment('1').'profile/info') ?>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label class="control-label">Username </label>
                                            <input type="text" readonly placeholder="<?php echo $prof->username; ?>" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <label class="control-label">First Name</label><?php echo form_error('first_name','<label style="color:red;">','</label>') ?>
                                            <input type="text" name="first_name" placeholder=" <?php echo ucwords($prof->first_name); ?>" value="<?php echo ucwords($prof->first_name); ?>" class="form-control"/>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="control-label">Last Name</label><?php echo form_error('last_name','<label style="color:red;">','</label>') ?>
                                            <input type="text" name="last_name" placeholder=" <?php echo ucwords($prof->last_name); ?>" value="<?php echo ucwords($prof->last_name); ?>" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <label class="control-label">Email </label><?php echo form_error('email','<label style="color:red;">','</label>') ?>
                                            <input type="email" name="email" placeholder="Add your email" value="<?php echo $prof->email ?>" class="form-control"/>
                                        </div>

                                        <div class="col-md-6">
                                            <label class="control-label">Phone </label> <?php echo form_error('phone','<label style="color:red;">','</label>') ?>
                                            <input type="text" name="phone" placeholder="Add Your Mobile Phone" value="<?php echo $prof->phone; ?>" class="form-control"/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label class="control-label">Current Password </label> <?php echo form_error('password','<label style="color:red;">','</label>') ?>
                                            <input type="password" name="password" autocomplete="off" placeholder="What is your current password ?"  class="form-control"/>
                                        </div>
                                    </div>

                                </div>

                                <div class="row pull-right" style="margin-top: 10px;">

                                    <div class="col-md-12">
                                        <button  class="btn btn-success" type="submit"> Update Changes </button>
                                        <button type="reset" class="btn default">
                                            Cancel </button>
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                            <!-- END PERSONAL INFO TAB -->

                            <!-- CHANGE AVATAR TAB -->
                            <div class="tab-pane fade <?php echo $subtitle=='change_avatar'?'active':''; ?>" id="tab_1_2">
                                <p>
                                    You Can Make Changes to the photo.
                                </p>
                                <?php echo form_open_multipart($this->uri->slash_segment('1').'profile/change_avatar')?>
                                <div class="form-group">

                                    <?php if( isset($error)){?>
                                        <span class="font-red-mint" >
                                        <?php echo  $error; ?>

                                    </span>
                                    <?php } echo form_error('image','<label style="color:red;">','</label>') ?>

                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                            <img src="<?php echo strlen($prof->photo)>0?base_url().$prof->photo:base_url().'assets/assets/img/avatars/placeholder.png' ?>" alt=""/>
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                                        </div>
                                        <div>
																<span class="btn default btn-file">
																<span class="fileinput-new">
																Select image </span>
																<span class="fileinput-exists">
																Change </span>
																<input type="file" name="image" required>
																</span>
                                            <a href="#" class="btn default fileinput-exists" data-dismiss="fileinput">
                                                Remove </a>
                                        </div>
                                    </div>

                                </div>

                                <?php echo form_error('pa','<label style="color:red;">','</label>') ?>
                                <input type="hidden" name="pa"  placeholder="What is your current password ?"  class="form-control"/>

                                <div class="margin-top-10">

                                    <button type="submit" class="btn green-jungle">Upload <i class="fa fa-upload"></i></button>
                                    <a href="#" class="btn default">
                                        Cancel </a>
                                </div>
                                <?php echo form_close() ?>
                            </div>
                            <!-- END CHANGE AVATAR TAB -->
                            <!-- CHANGE PASSWORD TAB -->
                            <div class="tab-pane fade <?php echo $subtitle=='change_password'?'active':''; ?>" id="tab_1_3">
                                <?php echo form_open($this->uri->slash_segment('1').'profile/change_password') ?>
                                <div class="form-group">
                                    <label class="control-label">Current Password </label><?php echo form_error('current_password','<label style="color: red;">','</label>') ?>
                                    <input name="current_password" autocomplete="off" type="password" class="form-control"/>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">New Password </label><?php echo form_error('new_pass','<label style="color: red;">','</label>') ?>
                                    <input name="new_pass" autocomplete="off" type="password" class="form-control"/>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Re-type New Password </label><?php echo form_error('rpt_pass','<label style="color: red;">','</label>') ?>
                                    <input name="rpt_pass" autocomplete="off" type="password" class="form-control"/>
                                </div>
                                <div class="margin-top-10 pull-right">
                                    <button type="submit" class="btn btn-success">
                                        Change Password </button>
                                    <button type="reset" class="btn default">
                                        Cancel </button>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                            <!-- END CHANGE PASSWORD TAB -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END PROFILE CONTENT -->

    <div/>

    <!-- BEGIN CORE PLUGINS -->
    <script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>

    <!-- END CORE PLUGINS -->

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>

    <!--    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <!--    <script src="../assets/pages/scripts/profile.min.js" type="text/javascript"></script>-->
    <!--    <!-- END PAGE LEVEL SCRIPTS -->



