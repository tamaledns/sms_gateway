

<div class="panel panel-flat">
    <div class="panel-heading ">
        <h5 class="panel-title">Account Info </h5>
        <div class="heading-elements">
            <ul class="icons-list">

                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li>
                <li><a data-action="close"></a></li>
            </ul>
        </div>
    </div>

   <div class="panel-body">
       <?php echo form_open($this->uri->slash_segment('1').'profile/info') ?>
       <div class="row">
           <div class="form-group">
               <div class="col-md-12">
                   <label class="control-label">Username </label>
                   <input type="text" readonly placeholder="<?php echo $prof->username; ?>" class="form-control"/>
               </div>
           </div>
           <div class="form-group">
               <div class="col-md-6">
                   <label class="control-label">First Name</label><?php echo form_error('first_name','<label style="color:red;">','</label>') ?>
                   <input type="text" name="first_name" placeholder=" <?php echo ucwords($prof->first_name); ?>" value="<?php echo ucwords($prof->first_name); ?>" class="form-control"/>
               </div>
               <div class="col-md-6">
                   <label class="control-label">Last Name</label><?php echo form_error('last_name','<label style="color:red;">','</label>') ?>
                   <input type="text" name="last_name" placeholder=" <?php echo ucwords($prof->last_name); ?>" value="<?php echo ucwords($prof->last_name); ?>" class="form-control"/>
               </div>
           </div>
           <div class="form-group">
               <div class="col-md-6">
                   <label class="control-label">Email </label><?php echo form_error('email','<label style="color:red;">','</label>') ?>
                   <input type="email" name="email" placeholder="Add your email" value="<?php echo $prof->email ?>" class="form-control"/>
               </div>

               <div class="col-md-6">
                   <label class="control-label">Phone </label> <?php echo form_error('phone','<label style="color:red;">','</label>') ?>
                   <input type="text" name="phone" placeholder="Add Your Mobile Phone" value="<?php echo $prof->phone; ?>" class="form-control"/>
               </div>
           </div>

           <div class="form-group">
               <div class="col-md-12">
                   <label class="control-label">Current Password </label> <?php echo form_error('password','<label style="color:red;">','</label>') ?>
                   <input type="password" name="password" autocomplete="off" placeholder="What is your current password ?"  class="form-control"/>
               </div>
           </div>

       </div>

       <div class="row pull-right" style="margin-top: 10px;">

           <div class="col-md-12">
               <button  class="btn btn-success" type="submit"> Update Changes </button>
               <button type="reset" class="btn default">
                   Cancel </button>
           </div>
       </div>
       <?php echo form_close(); ?>




   </div>
</div>


