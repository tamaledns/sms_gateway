<!-- Theme JS files -->
<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/uploaders/fileinput/plugins/purify.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/uploaders/fileinput/plugins/sortable.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/uploaders/fileinput/fileinput.min.js"></script>

<!--<script type="text/javascript" src="assets/js/core/app.js"></script>-->
<script type="text/javascript" src="<?= base_url() ?>assets/js/pages/uploader_bootstrap.js"></script>
<!-- /theme JS files -->



<div class="row">

    <div class="col-md-6">

        <!-- BEGIN SAMPLE FORM panel-->
        <div class="panel panel-flat bordered">
            <div class="panel-heading">

                <div class="panel-title">
                <h5>
                    <span class="caption-subject bold uppercase">New Group</span>
                </h5>
            </div>
                <div class="actions hidden">

                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="panel-body form">

                <?php echo form_open_multipart('') ?>
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-12">


                            <div class="row">


                                <div class="col-md-12" <?= $this->session->user_type==1?'':'hidden'; ?>>
                                    <div class="form-group form-md-line-input form-md-floating-label">

                                        <select class="select" name="owner">
                                            <option value="" <?= set_select('owner','',true) ?>>Owner</option>

                                            <?php foreach ($this->db->select()->from('users')->get()->result() as $owner): ?>
                                            <option value="<?= $owner->id ?>" <?= set_select('owner', $owner->id, $owner->id==$this->session->id?true:'') ?>><?= $owner->first_name.' '.$owner->last_name ?></option>
                                            <?php endforeach; ?>

                                        </select>


                                        <label for="form_control_1">Owner <?php echo form_error('name','<span style=" color:red;">','</span>') ?></label>

                                    </div>
                                </div>



                                <div class="col-md-12">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input type="text" class="form-control" name="name" value="<?php echo set_value('name') ?>">
                                        <label for="form_control_1">Group Name <?php echo form_error('name','<span style=" color:red;">','</span>') ?></label>

                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group form-md-line-input form-md-floating-label">

                                        <textarea class="form-control" name="desc"><?php echo set_value('desc') ?></textarea>


                                        <label for="form_control_1">Group Description <?php echo form_error('desc','<span style=" color:red;">','</span>') ?></label>

                                    </div>
                                </div>



                                <div class="form-group">

                                    <div class="col-lg-12">
                                        <input type="file" name="file_upload" class="file-input-csv-excel" data-browse-class="btn btn-primary btn-block" data-show-remove="false" data-show-caption="false" data-show-upload="false">
                                        <label class="control-label">Block button:</label>
                                        <span class="help-block">Upload "xlsx", "xls", "csv" files only.</span>
                                    </div>
                                </div>






                            </div>

                        </div>




                    </div>



                </div>
                <div class="form-actions">
                    <button type="submit"  class="btn btn-primary"><i class="icon-plus2"></i> Save</button>

                    <button type="reset" class="btn btn-default pull-right"> Cancel</button>
                </div>
                <?php echo form_close() ?>
            </div>
        </div>
        <!-- END SAMPLE FORM panel-->

    </div>
</div>

