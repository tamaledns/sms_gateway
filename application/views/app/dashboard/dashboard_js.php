<script src="<?= base_url() ?>assets/highcharts/highcharts.js"></script>
<!--<script src="https://code.highcharts.com/highcharts-3d.js"></script>-->
<script src="<?php echo base_url() ?>assets/highcharts/highcharts-3d.js"></script>
<script src="<?= base_url() ?>assets/highcharts/exporting.js"></script>
<!--<script src="--><? //= base_url() ?><!--assets/highcharts/themes/dark-green.js"></script>-->
<?php

$ldate = isset($last_day) ? date($last_day) : date('Y-m-d');
$period =period($fdate,$ldate);

?>

<script type="text/javascript">

    $(document).ready(function () {

    $('#delivered_sms').highcharts({
        colors: ["#EC407A", "#324490", "#0033ff", "#ff0000", "#aaeeee"],
        chart: {
            zoomType: 'x',
            type: 'spline'
        },
        plotOptions: {
            spline: {
                lineWidth: 2,
                states: {
                    hover: {
                        lineWidth: 2
                    }
                },
                marker: {
                    enabled: false
                }
            }
        },
        title: {
            text: '  Daily SMS from <?php echo date("d F Y", strtotime($fdate))  ?> <?php echo isset($last_day) ? ' - ' . date("d F Y", strtotime($last_day)) : ''; ?>',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: [

                <?php
                foreach ($period as $dt):

                    echo "'" . date('D, d M', strtotime($dt->format("Y-m-d"))) . "',";

                endforeach;
                ?>
            ]

        },
        yAxis: {
            title: {
                text: 'SMS'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            align: 'right',
            verticalAlign: 'top',
            y: 50,
            floating: true,
            borderWidth: 0
        },
        series: [
            {
                name: 'Delivered',
                data: [

                    <?php
                    foreach ($period as $dt) {

                        $table='inbox_' . date('dmY',strtotime($dt->format("Y-m-d")));

                       // echo $table;
                        $this->auto_create_table->create_inbox_table($table);

                        $this->db->select_sum('content_count');
                      $this->db
                            ->group_start()
                            ->where(
                                    'message_status','DELIVRD'
                            )
                            ->group_end();

                        $this->session->user_type!=1?$this->db->where('username',$this->session->id):'';

                        $a = $this->db->from($table)->get()->row();
                        echo round($a->content_count, 2) . ",";


                    }
                    ?>
                ]
            },

            {
                name: 'Un-Delivered',
                data: [

                    <?php
                    foreach ($period as $dt) {
                        $table='inbox_' . date('dmY',strtotime($dt->format("Y-m-d")));
                        $this->db->select_sum('content_count');
                         $this->db
                            ->group_start()
                            ->where(
                                'message_status !=','DELIVRD'
                            )->or_where(
                                'message_status is NULL'
                            )
                            ->group_end();

                        $this->session->user_type!=1?$this->db->where('username',$this->session->id):'';

                        $a =$this->db->from($table)->get()->row();

                        echo round($a->content_count, 2) . ",";


                    }
                    ?>
                ]
            },
            {
                name: 'Sent',
                data: [

                    <?php
                    foreach ($period as $dt) {
                        $table='inbox_' . date('dmY',strtotime($dt->format("Y-m-d")));
                        $this->db->select_sum('content_count');
                        $this->db
                            ->group_start()
                            ->where(
                                'status','Success'
                            )
                            ->group_end();

                        $this->session->user_type!=1?$this->db->where('username',$this->session->id):'';

                        $a = $this->db->from($table)->get()->row();

                        echo round($a->content_count, 2) . ",";


                    }
                    ?>
                ]
            }

//            ,{
//                name: 'Berlin',
//                data: [-0.9, 0.6, 3.5, 8.4, 13.5, 17.0, 18.6, 17.9, 14.3, 9.0, 3.9, 1.0]
//            }, {
//                name: 'London',
//                data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
//            }
        ]
    });


    $('#sent').highcharts({
        colors: ["#EC407A", "#324490", "#0033ff", "#ff0000", "#aaeeee"],
        chart: {

            zoomType: 'x',
            type: 'spline'


        },
        plotOptions: {
            spline: {
                lineWidth: 2,
                states: {
                    hover: {
                        lineWidth: 2
                    }
                },
                marker: {
                    enabled: false
                }
            }
        },

        title: {
            text: '  Daily Sent SMS from <?php echo date("d F Y", strtotime($fdate))  ?> <?php echo isset($last_day) ? ' - ' . date("d F Y", strtotime($last_day)) : ''; ?>',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: [

                <?php
                foreach ($period as $dt):

                    echo "'" . date('D, d M', strtotime($dt->format("Y-m-d"))) . "',";

                endforeach;
                ?>
            ]

        },
        yAxis: {
            title: {
                text: 'SMS'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            align: 'right',
            verticalAlign: 'top',
            y: 50,
            floating: true,
            borderWidth: 0
        },
        series: [
            {

                name: 'Outbox Messages',
                data: [

                    <?php
                    foreach ($period as $dt) {

                        $this->db->where(array('created_on >' => strtotime($dt->format("Y-m-d 00:00:00")), 'created_on <=' => strtotime($dt->format("Y-m-d 23:59:59"))));

                        $this->session->user_type!=1?$this->db->where(array('created_by'=>$this->session->id)):'';

                        $a = $this->db->from('outbox')->count_all_results();

                        echo round($a, 2) . ",";


                    }
                    ?>
                ]
            }

//            ,{
//                name: 'Berlin',
//                data: [-0.9, 0.6, 3.5, 8.4, 13.5, 17.0, 18.6, 17.9, 14.3, 9.0, 3.9, 1.0]
//            }, {
//                name: 'London',
//                data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
//            }
        ]
    });


    });

</script>


<!--<script src="--><?php //echo base_url() ?><!--assets/horizontal_bar/build/js/jquery.min.js"></script>-->
<script src="<?= base_url() ?>assets/horizontal_bar/build/js/jquery.horizBarChart.min.js"></script>

<script>
    $(document).ready(function () {
        $('.chart').horizBarChart({
            selector: '.bar',
            speed: 1000
        });
    });
</script>

