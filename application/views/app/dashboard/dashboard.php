<!-- Theme JS files -->
<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/visualization/d3/d3.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/forms/styling/switchery.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/forms/styling/uniform.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/ui/moment/moment.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/pickers/daterangepicker.js"></script>

<!--<script type="text/javascript" src="--><?//= base_url() ?><!--assets/js/core/app.js"></script>-->
<!--<script type="text/javascript" src="--><?//= base_url() ?><!--assets/js/pages/dashboard.js"></script>-->
<?php $this->load->view('app/dashboard/dashboard_limitless'); ?>
<?php $this->load->view('app/dashboard/dashboard_js') ?>
<!-- /theme JS files -->
<?php

$begin = new DateTime($fdate );
$ldate=isset($last_day)?date($last_day):date('Y-m-d');
$end = new DateTime( $ldate );

?>

<style>
    .pull-right{
        padding-right: 15px;
    }

    .blue-replace{
        background-color: #0000CC;
    }
    .pink-replace{
        background-color: #FFBF2B;
        border-color: #FFBF2B
    }
    .text-pink-replace{
        color: #FFBF2B;
    }
</style>


<?php $this->load->view('app/dashboard/boxes'); ?>
<?php $this->load->view('app/dashboard/content'); ?>
<?php //$this->load->view('app/dashboard/row_two'); ?>
<?php //$this->load->view('app/dashboard/row_four'); ?>
<?php //$this->load->view('app/dashboard/row_three'); ?>
<?php //$this->load->view('app/dashboard/row_last'); ?>
<?php $this->load->view('app/dashboard/ignore'); ?>
