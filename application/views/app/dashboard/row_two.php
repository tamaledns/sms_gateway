
<?php

$status_list = array(
    array("success" => "Pending"),
    array("info" => "Closed"),
    array("danger" => "On Hold"),
    array("warning" => "Fraud"),
    array("primary" => "Fraud"),
    array("pink" => "Fraud"),
    array("violet" => "Fraud")
);


$patient_type = array(
    array("default" => "N/A"),
    array("default" => "Child Under 5yr"),
    array("success" => "Pregnant Woman"),
    array("primary" => "Other")
);


$referral_confirm = array(
    array("default" => "Pending"),
    array("success" => "Accepted"),
    array("danger" => "Reassigned"),
    array("primary" => "Other")
);


?>

<?php $vht_referrals =$this->dashboard_model->vht_referrals(); ?>
<?php $hf_referrals =$this->dashboard_model->hf_referrals(); ?>
<!-- Dashboard content -->
<style>


    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: 12px 12px !important;
        line-height: 1.5384616;
        vertical-align: top;
        border-top: 1px solid #ddd;
    }
</style>
<div class="  row">
    <div class="col-lg-6">

        <!-- Marketing campaigns -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title">Health Facility Referrals</h6>

            </div>

            <div class="table-responsive   pre-scrollabl">
                <table class="table text-nowrap">
                    <thead>
                    <tr>
                        <th >Patient Name</th>
<!--                        <th>Phone </th>-->

<!--                        <th>Patient Type</th>-->
                        <th>Status</th>
                        <th>Date</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php foreach ($hf_referrals as $m):
                        $status = $status_list[rand(0, 6)];

                        $patient_typ = $patient_type[$m->patient_type];
                        $referral_conf = $referral_confirm[$m->referral_confirm];
                        ?>

                        <?php
                        $rd = $this->model->get_facility_by_id($m->referral_destination);
                        $referral_destination = $rd != false ? $rd->name : $m->referral_destination;
                        ?>


                        <tr>
                        <td>
                            <div class="media-left media-middle">
                                <a href="#" class="btn bg-<?= key($status) ?>-400 btn-rounded btn-icon btn-xs">
                                    <span class="letter-icon"></span>
                                </a>
                            </div>

                            <div class="media-body">
                                <div class="media-heading">
                                    <a href="#" class="letter-icon-title"  data-popup="tooltip" title="<?= $m->patient_name ?>" data-placement="top"  ><?= character_limiter($m->patient_name,25) ?></a>
                                </div>

                                <div class="text-muted text-size-small"><i class="icon-earth text-size-mini position-left"></i> <?= $referral_destination ?></div>
                            </div>
                        </td>

                        <td>
                            <h6 class="text-semibold no-margin"> <?= '<span class="label label-' . (key($referral_conf)) . '" style="width:100px;">' . current($referral_conf) . '</span>' ?></h6>
                        </td>
                        <td>
                            <h6 class="text-semibold no-margin"><?=  time_elapsed_string(date('Y-m-d H:i:s', $m->created_on)) ?></h6>
                        </td>
                    </tr>

                    <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
        <!-- /marketing campaigns -->

    </div>

    <div class="col-lg-6">

        <!-- Marketing campaigns -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title">VHT Referrals</h6>

            </div>

            <div class="table-responsive   pre-scrollabl">
                <table class="table text-nowrap">
                    <thead>
                    <tr>
                        <th>Household</th>
                        <th>Phone</th>
                        <th>Illness</th>
                        <th>Date</th>



                    </tr>
                    </thead>
                    <tbody>

                    <?php foreach ($vht_referrals as $m):
                        $status = $status_list[rand(0, 6)];

                        ?>

                        <?php
                        $rd = $this->model->get_facility_by_id($m->referral_destination);
                        $referral_destination = $rd != false ? $rd->name : $m->referral_destination;

                        $ci = $this->ussd_model->get_illness_option('pregnancy_complications',$m->complications);


                        ?>

                        <tr>
                            <td>
                                <div class="media-left media-middle">
                                    <a href="#" class="btn bg-<?= key($status) ?>-400 btn-rounded btn-icon btn-xs">
                                        <span class="letter-icon"></span>
                                    </a>
                                </div>

                                <div class="media-body">
                                    <div class="media-heading">
                                        <a href="#" class="letter-icon-title"  data-popup="tooltip" title="<?= $m->full_name ?>" data-placement="top"  ><?= $m->full_name ?>(<?= $m->household_code ?>)
                                        </a>
                                    </div>

                                    <div class="text-muted text-size-small"><i class="icon-earth text-size-mini position-left"></i> <?= $referral_destination ?></div>
                                </div>
                            </td>




                            <td>

                                <?php  $pw = $this->model->get_pregnant_woman($m->pregnant_woman_no); ?>

                                <h6 class="text-semibold no-margin"><?= (isset($pw->pregnant_woman_name)?$pw->pregnant_woman_name:'N/A') ?></h6>
                            </td>
                            <td>
                                <h6 class="text-semibold no-margin"><?= $ci->illness ?></h6>
                            </td>

                            <td>
                                <h6 class="text-semibold no-margin"><?=  time_elapsed_string(date('Y-m-d H:i:s', $m->created_on)) ?></h6>
                            </td>


                        </tr>

                    <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
        <!-- /marketing campaigns -->

    </div>


</div>
<!-- /dashboard content -->
