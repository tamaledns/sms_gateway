

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="panel panel-white">
            <div class="panel-heading">
            <div class="panel-title">
                <div class="caption font-green-haze">
                    <i class="icon-key font-green-haze"></i>
                    <span class="caption-subject bold uppercase"> New Permission</span>
                </div>


            </div>

                <div class="heading-elements">
                    <ul class="icons-list">

                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>

        </div>
            <div class="panel-body form">

                <?php echo form_open($this->page_level.$this->page_level2.'new/'.(isset($id)?$id*date('Y'):'' ),array('class'=>'form-horizontal')) ?>

                <div class="form-body">



                    <div class="form-group form-md-line-input">
                        <label class="col-md-2 control-label" for="form_control_1">Permission</label>
                        <div class="col-md-10">
                           <input required class="form-control" type="text" name="permission"
                                  value="<?php echo set_value('permission',(isset($id)?$perm->title:'')) ?>
">
                            <label for="form_control_1"> <?php echo form_error('permission','<span style=" color:red;">','</span>') ?></label>
                            <span class="help-block">Insert Permission</span>
                        </div>

                    </div>


                    <div class="form-group form-md-line-input">
                        <label class="col-md-2 control-label" for="form_control_1">Group</label>
                        <div class="col-md-10">
                           <input required class="form-control" type="text" name="group" value="<?php echo set_value('group',(isset($id)?$perm->perm_group:'')) ?>">
                            <label for="form_control_1"> <?php echo form_error('group','<span style=" color:red;">','</span>') ?></label>
                            <span class="help-block">Group</span>
                        </div>

                    </div>


                    <div class="form-group form-md-line-input">
                        <label class="col-md-2 control-label" for="form_control_1">Description</label>
                        <div class="col-md-10">
                            <textarea class="form-control" name="description"><?php echo set_value('description',(isset($id)?$perm->perm_desc:'')) ?></textarea>
                            <label for="form_control_1"> <?php echo form_error('description','<span style=" color:red;">','</span>') ?></label>
                            <span class="help-block">Insert Description about the Permission</span>
                        </div>

                    </div>



                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-10 col-md-12">
                            <button type="reset" class="btn default"> <i class="fa fa-remove"></i> Cancel</button>
                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->

    </div>


</div>