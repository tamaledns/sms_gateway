
<?php

$permissions=$this->db->select()->from('permissions')->where('perm_group',$perm_group)->get()->result();

if(empty($permissions)){
    $data['alert'] = 'info';
    $data['message'] = 'Permissions Not found !!';


    $this->load->view('alert', $data);
}else{ ?>

<!-- Contextual colors -->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title"><?= humanize($permissions[0]->perm_group) ?></h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                Below are the permissions in the permission group.
            </div>

            <div class="table-responsive">
                <table class="table table-lg">
                    <thead>
                    <tr>
                        <th>Permission</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>


                    <?php
                    $no=1;
                    foreach($permissions as $t): ?>



                        <tr>
                            <td><?php echo anchor($this->page_level.$this->page_level2.'edit/'.$t->id*date('Y'),$t->title) ?></td>
                            <td><?php echo $t->perm_desc ?></td>
                            <td style="width: 80px;">



                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li>
                                                <?php echo anchor($this->page_level.$this->page_level2.'edit/'.$t->id*date('Y'),'  <i class="fa fa-eye"></i> View') ?>
                                            </li>
                                            <li>
                                                <?php echo anchor($this->page_level.$this->page_level2.'edit/'.$t->id*date('Y'),'  <i class="fa fa-pencil"></i> Edit') ?>
                                            </li>
                                            <li>
                                                <?php echo anchor($this->page_level.$this->page_level2.'delete/'.$t->id*date('Y').'/'.$this->uri->segment(4),'  <i class="icon-database-remove"></i> Delete','onclick="return confirm(\'Deleting this Permission will also Delete the roles its assigned to\n\r Are you sure you want to delete ?\')"') ?>
                                            </li>

                                        </ul>
                                    </li>
                                </ul>




                            </td>
                        </tr>



                        <?php $no++; endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /contextual colors -->

<?php } ?>