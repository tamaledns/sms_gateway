
<!-- BEGIN PAGE CONTENT-->
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />

<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="panel panel-flat">
            <div class="panel-heading">
            <div class="panel-title">


                <div class="caption font-dark hidden-print">


                    <?php echo form_open('','class="form-inline" ')   ?>
                    <?php echo anchor($this->page_level.$this->page_level2.'new',' <i class="fa fa-plus"></i> New','class="btn btn-sm btn-primary"'); ?>
                    <div class="form-group hidden">
                        <div class="">

                            <div class="input-group">
                                <select class="form-control input-sm" name="status" >
                                    <option value="" <?php echo set_select('status','',true) ?>>Select Status</option>
                                    <option value="1" <?php echo set_select('status','1') ?>>Active</option>
                                    <option value="2" <?php echo set_select('status','2') ?>>Blocked</option>

                                </select>
                            </div>
                            <button class="btn green btn-sm" type="submit"><i class="fa fa-sliders"></i> Apply</button></div>
                    </div>

                    <?php echo form_close(); ?>

                </div>
                <div class="tools"> </div>
            </div>
        </div>
            <div class="panel-body">
                <table class="table  datatable-button-init-basic" id="sample_1">

                    <thead>
                    <tr>
                        <th style="width: 5%;">#</th>
                        <th> Group </th>
                        <th> Permissions </th>


                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php


                    $permissions=$this->db->select('id,perm_group,count(perm_group) as permissions')->from('permissions')->group_by('perm_group')->get()->result();
                    $no=1;
                    foreach($permissions as $t): ?>
                    <tr>
                        <td><?php echo $no; ?></td>

                        <td style="white-space: nowrap">

                            <?php echo anchor($this->page_level.$this->page_level2.'perm_group/'.str_rot13(underscore($t->perm_group)),$t->perm_group) ?>
                        </td>
                        <td>
                            <?= $t->permissions ?>


                        </td>


                        <td style="width: 80px;">



                            <ul class="icons-list">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="icon-menu9"></i>
                                    </a>

                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li>
                                            <?php echo anchor($this->page_level.$this->page_level2.'perm_group/'.str_rot13(underscore($t->perm_group)),'  <i class="fa fa-eye"></i> View') ?>
                                        </li>

                                    </ul>
                                </li>
                            </ul>




                        </td>
                    </tr>
                    <?php $no++; endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>
<!-- END PAGE BASE CONTENT -->

