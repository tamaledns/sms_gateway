<?php


$limit = 500;

$this->db->select('a.*')->from('contacts a');

if ($this->input->post('groups')) {
    $this->db->where(array('a.group_id' => $id));
}

$this->session->user_type != 1 ? $this->db->where('a.created_by', $this->session->id) : '';

isset($limit) ? $this->db->limit($limit) : '';

$contacts = $this->db->get()->result();


?>

<?php //print_r($this->input->post()); ?>

<!-- BEGIN PAGE CONTENT-->
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"
      rel="stylesheet" type="text/css"/>

<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE panel-->
        <?php echo form_open($this->page_level . $this->page_level2, 'class="form-inline" ') ?>
        <div class="panel panel-flat bordered">
            <div class="panel-heading">


                <div class="panel-title">

                    <div class="caption font-dark hidden-print">


                        <?php echo anchor($this->page_level . $this->page_level2 . 'new', ' <i class="fa fa-plus"></i> New Contact', 'class="btn btn-sm btn-success"'); ?>
                        <div class="form-group">
                            <div class="">
                                <div class="input-group">

                                    <select class="form-control input-sm" name="groups">

                                        <option value="" <?php echo set_select('groups', '', true) ?>>Groups</option>

                                        <?php

                                        $groups = $this->model->get_groups();

                                        echo count($groups) == 0 ? '<option value="">No groups</option>' : '';

                                        foreach ($groups as $u): ?>


                                            <option value="<?php echo $u->id ?>" <?= $u->id == $this->input->post('groups') ? 'selected' : '' ?>><?php echo ucwords($u->name) ?></option>
                                        <?php endforeach; ?>
                                    </select>

                                </div>

                                <div class="input-group hidden">

                                    <select class="form-control input-sm" name="group_action">

                                        <option value="sms" <?php echo set_select('group_action', 'sms', true) ?>>Send
                                            SMS
                                        </option>
                                        <option value="email" <?php echo set_select('group_action', 'email', true) ?>>
                                            Send Email
                                        </option>
                                        <option value="mail_sms" <?php echo set_select('group_action', 'mail_sms', true) ?>>
                                            Send Mail + SMS
                                        </option>

                                    </select>

                                </div>

                                <button class="btn btn-default btn-sm" type="submit" name="filter"><i
                                            class="fa fa-sliders"></i> Apply
                                </button>


                                <?php echo anchor($this->page_level . 'groups/new', ' <i class="icon-users"></i> New Group', 'class="btn btn-sm btn-primary"'); ?>

                                <!--                         <button class="btn yellow-gold btn-sm" value="send_sms" type="submit" name="send_sms"><i class="icon-paper-plane"></i> Send SMS</button>-->
                                <!--                       <button class="btn red-thunderbird btn-sm" value="send_mail" type="submit" name="send_mail"><i class="icon-envelope"></i> Send Mail</button>-->
                            </div>
                        </div>


                    </div>
                </div>
                <div class="tools"></div>
            </div>

            <table class="table  datatable-button-init-basic  table-striped table-bordered  table-checkable  table-hover">

                <thead>
                <tr>
                    <th width="2%" class="hidden-print">
                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                            <input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes"/>
                            <span></span>
                        </label>
                    </th>


                    <th width="2">#</th>
                    <th> Name</th>

                    <th> Phone</th>
                    <!--                        <th style="width: 8px;">Status</th>-->
                    <th style="width: 8px;" class="hidden-print"><i class="icon-menu9"></i></th>
                </tr>
                </thead>
                <tbody>
                <?php


                $no = 1;
                foreach ($contacts as $user): ?>
                    <tr>

                        <td class="hidden-print">
                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]"
                                                                                                     type="checkbox"
                                                                                                     class="checkboxes"
                                                                                                     value="<?= $user->id ?>"/><span></span></label>
                        </td>

                        <td><?php echo $no; ?></td>


                        <td>

                            <?= ucwords($user->first_name . ' ' . $user->last_name) ?>
                        </td>


                        <td> <?php echo $user->phone_no ?> </td>


                        <!--                        <td hidden>-->
                        <!--                            --><?php //$status=$user->status ?>
                        <!---->
                        <!--                            <div class="btn btn-sm btn--->
                        <?php //echo $status=='2'?'danger':'success';  ?><!--">-->
                        <?php //echo $status=='2'?'Blocked':'Active';  ?><!--</div>-->
                        <!--                        </td>-->
                        <td style="width: 80px;" class="hidden-print">
                            <div class="btn-group">
                                <a class="btn green-jungle btn-sm" href="javascript:;" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <?php echo anchor($this->page_level . $this->page_level2 . 'edit/' . $user->id * date('Y'), '  <i class="fa fa-eye"></i> View') ?>
                                    </li>


                                    <!--                                    Sending message-->
                                    <li>
                                        <?php echo anchor('ajax_api/message_form/' . $user->id * date('Y'), '<i class="fa fa-edit"></i>  Send SMS', 'data-target="#ajax" data-toggle="modal"') ?>
                                    </li>
                                    <li>
                                        <?php echo anchor('ajax_api/message_form/' . $user->id * date('Y'), '<i class="fa fa-edit"></i>  Send Email', 'data-target="#ajax" data-toggle="modal"') ?>
                                    </li>
                                    <li>
                                        <?php echo anchor('ajax_api/message_form/' . $user->id * date('Y'), '<i class="fa fa-edit"></i>  Send Email + SMS', 'data-target="#ajax" data-toggle="modal"') ?>
                                    </li>

                                    <!--                                    Sending Message-->
                                    <li>
                                        <?php echo anchor($this->page_level . $this->page_level2 . 'edit/' . $user->id * date('Y'), '  <i class="fa fa-pencil"></i> Edit') ?>
                                    </li>
                                    <?php if ($this->session->userdata('user_type') != '2') { ?>
                                        <li>

                                            <?php echo anchor($this->page_level . $this->page_level2 . 'delete/' . $user->id * date('Y'), '  <i class="fa fa-trash-o"></i> Delete', 'onclick="return confirm(\'Are you sure you want to delete ?\')"') ?>
                                        </li>
                                        <li>

                                            <?php echo $user->status == 2 ? anchor($this->page_level . $this->page_level2 . 'unblock/' . $user->id * date('Y'), '  <i class="fa fa-check"></i> Unblock') : anchor($this->page_level . $this->page_level2 . 'ban/' . $user->id * date('Y'), '  <i class="fa fa-ban"></i> Block', 'onclick="return confirm(\'You are about to ban User from accessing the System \')"') ?>
                                        </li>
                                        <li class="divider">
                                        </li>

                                    <?php } ?>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    <?php $no++; endforeach; ?>

                </tbody>
            </table>


        </div>

        <?php echo form_close(); ?>
        <!-- END EXAMPLE TABLE panel-->

    </div>
</div>
<!-- END PAGE BASE CONTENT -->

