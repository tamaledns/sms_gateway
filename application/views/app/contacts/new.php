
<div class="row">

    <div class="col-md-8">

        <!-- BEGIN SAMPLE FORM panel-->
        <div class="panel panel-flat bordered">
            <div class="panel-heading">

                <div class="panel-title">
                <div class="caption font-red-sunglo">
                    <span class="caption-subject bold uppercase">New Contact</span>
                </div>
            </div>
                <div class="actions hidden">

                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="panel-body form">

                <?php echo form_open('') ?>
                <div class="form-body">
                    <div class="row">



                        <div class="col-md-12" hidden>
                            <div class="form-group form-md-line-input form-md-floating-label">

                                <select class="select" name="owner">
                                    <option value="" <?= set_select('owner','',true) ?>>Owner</option>

                                    <?php foreach ($this->db->select()->from('users')->get()->result() as $owner): ?>
                                        <option value="<?= $owner->id ?>" <?= set_select('owner', $owner->id, $owner->id==$this->session->id?true:'') ?>><?= $owner->first_name.' '.$owner->last_name ?></option>
                                    <?php endforeach; ?>

                                </select>


                                <label for="form_control_1">Owner <?php echo form_error('name','<span style=" color:red;">','</span>') ?></label>

                            </div>
                        </div>




                        <div class="col-md-12">


                            <div class="row">


                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input type="text" class="form-control" name="first_name" value="<?php echo set_value('first_name') ?>">
                                        <label for="form_control_1">First Name <?php echo form_error('first_name','<span style=" color:red;">','</span>') ?></label>

                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input type="text" class="form-control" name="last_name" value="<?php echo set_value('last_name') ?>">
                                        <label for="form_control_1">Last Name <?php echo form_error('last_name','<span style=" color:red;">','</span>') ?></label>

                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input type="email" class="form-control" name="email" value="<?php echo set_value('email') ?>" id="form_control_1">
                                        <label for="form_control_1">Email  <?php echo form_error('email','<span style=" color:red;">','</span>') ?></label>
                                        <span class="help-block">Add Email</span>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input type="phone" class="form-control" required name="phone" value="<?php echo set_value('phone') ?>" id="form_control_1">
                                        <label for="for m_control_1">Phone  <?php echo form_error('phone','<span style=" color:red;">','</span>') ?></label>
                                        <span class="help-block">Add phone</span>
                                    </div>
                                </div>



                            </div>

                        </div>




                    </div>



                </div>
                <div class="form-actions">
                    <button type="submit"  class="btn btn-success"><i class="icon-plus3"></i> Save</button>

                    <button type="reset" class="btn btn-default pull-right"> Cancel</button>
                </div>
                <?php echo form_close() ?>
            </div>
        </div>
        <!-- END SAMPLE FORM panel-->

    </div>
</div>

