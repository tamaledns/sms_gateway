
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />


<div class="row">

    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->

        <div class="panel panel-flat">

            <div class="panel-heading">
            <div class="panel-title">
                <div class="caption">


                    <div class="col-md" >
                        <?php echo form_open('','class="form-inline" ')   ?>
                        Logs
                        <div class="form-group hidden">
                            <div class="">
                                <div class="input-group  input-large date-picker input-daterange" data-date="<?php echo date('Y-m-d') ?>" data-date-format="yyyy-mm-dd">
                                    <span class="input-group-addon">From </span>
                                    <input type="text" class="form-control input-sm" name="from" value="<?php echo set_value('from') ?>">
                                    <span class="input-group-addon">to </span>
                                    <input type="text" class="form-control input-sm" name="to" value="<?php echo set_value('to')?>">
                                </div>
                                <input class="form-control input-inline  input-sm" name="search" type="text" style="width: 100px;" placeholder="Keyword..">
                                <select  class="select input-inline input-small input-sm" name="transaction_type">
                                    <option value="">Resource Type...</option>
                                    <?php foreach($this->db->select('distinct(transaction_type)')->from('logs')->get()->result() as $sc): ?>
                                        <option value="<?php echo $sc->transaction_type ?>" <?php echo set_select('transaction_type',$sc->transaction_type) ?>><?php echo humanize($sc->transaction_type) ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <button class="btn green btn-sm" type="submit"><i class="fa fa-sliders"></i> Apply</button></div>
                        </div>

                        <?php echo form_close(); ?>
                    </div>
                 </div>

                <div class="heading-elements">




                    <?php  echo anchor($this->page_level.$this->page_level2.'all','All','class="btn btn-default"') ?>
                    <div class="btn-group hidden">
                        <a class="btn btn-default" href="javascript:;" data-toggle="dropdown">
                            Transactions
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <?php echo anchor($this->page_level.$this->page_level2.'transfer','Transfer') ?>
                            </li>
                            <li>
                                <?php echo anchor($this->page_level.$this->page_level2.'cashout','Cashout') ?>
                            </li>
                            <li>
                                <?php echo anchor($this->page_level.$this->page_level2.'hold','Hold') ?>
                            </li>
                            <li>
                                <?php echo anchor($this->page_level.$this->page_level2.'canceled','Canceled') ?>
                            </li>
                            <li>
                                <?php echo anchor($this->page_level.$this->page_level2.'release','Release Transaction') ?>
                            </li>
                            <li>
                                <?php echo anchor($this->page_level.$this->page_level2.'approve','Approve Transaction') ?>
                            </li>

                        </ul>
                    </div>



                    <div class="btn-group hidden">
                        <a class="btn btn-default" href="javascript:;" data-toggle="dropdown">
                             Account
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <?php echo anchor($this->page_level.$this->page_level2.'account_creation','Account Creation') ?>
                            </li>
                            <li>
                                <?php echo anchor($this->page_level.$this->page_level2.'account_deletion','Account Deletion') ?>
                            </li>
                            <li>
                                <?php echo anchor($this->page_level.$this->page_level2.'account_modification','Account Modification') ?>
                            </li>
                            <li>   <?php echo anchor($this->page_level.$this->page_level2.'password_change','Password Change') ?></li>

                        </ul>
                    </div>


                    <?php  echo anchor($this->page_level.$this->page_level2.'realtime_monitoring','Realtime Monitoring','class="btn btn-default"') ?>
                </div>


            </div>

        </div>
            <div class="panel-body">




                <?php echo form_open('') ?>
                <table class="table datatable-basi" id="sample_">
                    <thead>
                    <tr>


                        <th style="width: 5%;">Date</th>
                        <th>User</th>
                        <th>Transaction Type</th>

                        <th>Details</th>
                        <th>IP address</th>
                        <th>Platform</th>

                        <th hidden>
                            <div class="btn-group">
                                <button class="btn btn-sm green dropdown-toggle" data-toggle="dropdown">Action <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <button type="submit" name="form_sub" value="approved" class="btn btn-default btn-sm col-sm-12" style="text-align:left; border: none;padding: 8px 14px;" onclick="return confirm('Are you sure you want Approve these Schools ?')" ><i class="fa fa-check"></i> Approve</button>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <button type="submit" name="form_sub" value="published" class="btn btn-sm btn-default col-sm-12" style="text-align:left; border: none;padding: 8px 14px;" onclick="return confirm('Are you sure you want Publish these schools ?')" ><i class="fa fa-desktop"></i> Publish</button>

                                    </li>
                                    <li>
                                        <button type="submit" name="form_sub" value="rejected" class="btn btn-sm btn-default col-sm-12" style="text-align:left; border: none;padding: 8px 14px;" onclick="return confirm('Are you sure you want Reject these schools ?')" ><i class="fa fa-ban"></i> Reject</button>

                                    </li>
                                    <!--                        echo form_submit('mysubmit', 'Submit Post!');-->

                                </ul>
                            </div>
                        </th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php

                    foreach($tlogs as $tl ): ?>
                        <tr class="odd gradeX">

                            <td><?php  $date=$tl->created_on;
                                echo date('Y-m-d') == date('Y-m-d', $date) ? date('H:i:s', $date) :'<span style="white-space: nowrap;">'. date('d-m-Y', $date).'</span><br/>'.date('H:i', $date).'hrs';
                                ?></td>
                            <td><?php echo humanize($tl->first_name.' '.$tl->last_name) ?></td>
                            <td><?php echo humanize($tl->transaction_type) ?></td>

                            <td><b> <?php echo humanize($tl->first_name.' '.$tl->last_name.' ') ?></b>  <?php echo $tl->details ?>
                                <b><?php echo $tl->target ?></b></td>
                            <td><?php echo $tl->ip ?></td>
                            <td style=" white-space: nowrap;"><?php echo $tl->platform ?></td>

                            <td hidden></td>
                        </tr>

                    <?php  endforeach; ?>
                    <!--                <tr><td colspan="5" class="portlet box purple" style="color: white;margin: 5px;">de</td></tr>-->
                    </tbody>
                </table>
                <?php echo form_close(); ?>
                <?php $this->load->view('pag'); ?>

            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>



<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->