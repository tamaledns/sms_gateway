

<div class="row">
    <div class="col-md-8">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title">Left side placement</h6>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                <div class="tabbable nav-tabs-vertical nav-tabs-left">
                    <ul class="nav nav-tabs nav-tabs-highlight" style="width: 200px;">
                        <li class="active"><a href="#identiy_card" data-toggle="tab"><i class="icon-menu7 position-left"></i> Identity Card</a></li>
                        <li><a href="#access_permit" data-toggle="tab"><i class="icon-mention position-left"></i> Access Permit</a></li>

                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active has-padding" id="identiy_card">
                            <?php $this->load->view('home/make_request/form'); ?>
                        </div>

                        <div class="tab-pane has-padding" id="access_permit">
                            <?php $this->load->view('home/make_request/form'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="panel panel-flat">

            <div class="panel-body">
        <!-- Latest updates -->

        <div class="sidebar-category">
            <div class="category-title">
                <span>Latest updates</span>
                <ul class="icons-list">
                    <li><a href="#" data-action="collapse"></a></li>
                </ul>
            </div>

            <div class="category-content">
                <ul class="media-list">
                    <li class="media">
                        <div class="media-left">
                            <a href="#" class="btn border-primary text-primary btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-pull-request"></i></a>
                        </div>

                        <div class="media-body">
                            Drop the IE <a href="#">specific hacks</a> for temporal inputs
                            <div class="media-annotation">4 minutes ago</div>
                        </div>
                    </li>

                    <li class="media">
                        <div class="media-left">
                            <a href="#" class="btn border-warning text-warning btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-commit"></i></a>
                        </div>

                        <div class="media-body">
                            Add full font overrides for popovers and tooltips
                            <div class="media-annotation">36 minutes ago</div>
                        </div>
                    </li>

                    <li class="media">
                        <div class="media-left">
                            <a href="#" class="btn border-info text-info btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-branch"></i></a>
                        </div>

                        <div class="media-body">
                            <a href="#">Chris Arney</a> created a new <span class="text-semibold">Design</span> branch
                            <div class="media-annotation">2 hours ago</div>
                        </div>
                    </li>

                    <li class="media">
                        <div class="media-left">
                            <a href="#" class="btn border-success text-success btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-merge"></i></a>
                        </div>

                        <div class="media-body">
                            <a href="#">Eugene Kopyov</a> merged <span class="text-semibold">Master</span> and <span class="text-semibold">Dev</span> branches
                            <div class="media-annotation">Dec 18, 18:36</div>
                        </div>
                    </li>

                    <li class="media">
                        <div class="media-left">
                            <a href="#" class="btn border-primary text-primary btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-pull-request"></i></a>
                        </div>

                        <div class="media-body">
                            Have Carousel ignore keyboard events
                            <div class="media-annotation">Dec 12, 05:46</div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /latest updates -->

            </div>

        </div>
    </div>

</div>
<!-- /vertical tabs -->

