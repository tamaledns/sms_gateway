<style>
    .widget-booking-form-wrapper #booking-tabs ul li a {
        display: block;
        float: left;
        width: calc(100% - 30px);
        padding: 25px 15px;
        color: #fff;
        text-align: center;
        position: relative;
    }

    .custom-block{
        border-left: solid thick #e03301;
        height: 95px;
        width: 250px;
        float: right;
        padding-left: 10px;
        border-style: dotted;

    }

    .header-booking-form-wrapper .booking-form-1, .widget-booking-form-wrapper .booking-form-1 {
        padding: 29px;
    }

    #page-header {
        background: url(<?= base_url('public_assets/images/image13.jpg') ?>) no-repeat center top #000056;
        width: 100%;
        height: 125px;
        padding: 50px 0 0 0;
        margin: -33px 0 10px 0;
    }

    #page-header h1 {
        text-align: center;
        font-size: 70px;
        color: #FFF;
        font-family: Sans-Serif;
    }

</style>


<div  id="page-header">
    <h1><?= strtoupper($this->site_options->title('site_name')) ?></h1>
    <div class="title-block3"></div>
<!--    <p><a href="#">Home</a><i class="fa fa-angle-right"></i>About Us</p>-->
</div>

    <!-- BEGIN .content-wrapper-outer -->
    <div class="content-wrapper-outer clearfix">

        <!-- BEGIN .main-content -->
        <div class="main-content">

            <ul class="link-blocks clearfix">



                <li style="height: 80px;">
                    <h3>
                        <?php echo anchor($this->page_level.'arrival_departure/new_arrival','<i class="icon-plane"></i>  <span class="link-text">Arrival & Departure</span><span class="link-arrow fa fa-angle-right"></span>','class="link-block-3"') ?>

                    </h3>

                    <div class="custom-block">

                        <!--                            <div class="widget-block"></div>-->

                        <!--                            Download Instruction-->
                        &nbsp;
                        <button type="submit" style="width: 100%; background:#e03301;" >
                            <i class="fa fa-download"></i> <span> Download Instructions</span>
                        </button>

                    </div>

                </li>



                <?php foreach ($this->db->select()->from('request_type')->order_by('priority','desc')->get()->result() as $rt): ?>

                    <li style="height: 80px;"><h3>
                        <?php echo anchor($this->page_level.'requests/'.$rt->request_page,'<i class="'.$rt->icon.'"></i>  <span class="link-text">'.$rt->title.'</span><span class="link-arrow fa fa-angle-right"></span>','class="link-block-3"') ?>

                        </h3>

                        <div class="custom-block">

<!--                            <div class="widget-block"></div>-->

<!--                            Download Instruction-->
&nbsp;
                            <button type="submit" style="width: 100%; background:#e03301;" >
                                <i class="fa fa-download"></i> <span> Download Instructions</span>
                            </button>

                        </div>

                    </li>



                <?php endforeach; ?>


            </ul>

            <!-- END .main-content -->
        </div>

        <!-- BEGIN .sidebar-content -->
        <div class="sidebar-content" style="margin-top: 30px;">

            <!-- BEGIN .widget-booking-form-wrapper -->
            <div class="widget-booking-form-wrapper">



                <!-- BEGIN #booking-tabs -->
                <div id="booking-tabs">

                    <ul class="nav clearfix">
                        <li><a href="#tab-one-way">Report Issue</a></li>

                    </ul>

                    <!-- BEGIN #tab-one-way -->
                    <div id="tab-one-way">

                        <!-- BEGIN .booking-form-1 -->
                        <form action="booking.html" class="booking-form-1" method="post">

                            <input type="text" name="app_id" value="" placeholder="Application ID" />
                            <input type="text" name="passport_id" value="" placeholder="Passport ID" />
                            <input type="text" name="Subject" class="datepickes" value="" placeholder="Subject" />

                           <textarea class="" style="width: 98%" rows="11"></textarea>



                            <button type="submit" >
                                <span>Send</span>
                            </button>

                            <!-- END .booking-form-1 -->
                        </form>

                        <!-- END #tab-one-way -->
                    </div>


                    <!-- END #booking-tabs -->
                </div>

                <!-- END .widget-booking-form-wrapper -->
            </div>



            <!-- END .sidebar-content -->
        </div>

        <!-- END .content-wrapper-outer -->
    </div>